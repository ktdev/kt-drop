<?php

//-----------------------------------------------------------------------------
 
    /* ++++++++++++++++++++++++++++ */
    /* +++  Constantes Globales +++ */
    /* ++++++++++++++++++++++++++++ */
    
    // Déclaration des variables globales
    global 
    $G_TXT_WELCOM , 
    $G_TXT_NOTE,
    $G_ROUTER,
    $G_PRISON,
    $G_TXT_PATH_INSTALL,
    $URI_ROOT;


    /* ++++++++++++++++++++++++++++++ */
    /* +++    Constantes System   +++ */
    /* ++++++++++++++++++++++++++++++ */

    // Définit le répertoire du module principal
    if (! defined ( 'D_PRIMARY' )) define ( 'D_PRIMARY', 'code' );

    // Définit le répertoire du module secondaire : Backend
    if (! defined ( 'D_SECONDARY' )) define ( 'D_SECONDARY', 'backend' );
    
    // Définit le répertoire du module tertiaire : Admin
    if (! defined ( 'D_TERTIARY' )) define ( 'D_TERTIARY', 'admin' );

    // Définit Module Home Folder
    if (! defined ( 'D_HOME' )) define ( 'D_HOME', 'home' );

    // Définit Module back Folder (Backend Management)
    if (! defined ( 'D_BACK' )) define ( 'D_BACK', 'back' );
    
    // Définit Module admin Folder (Administrator Management)
    if (! defined ( 'D_ADM' )) define ( 'D_ADM', 'adm' );
    
    // Définit Module user Folder 
    if (! defined ( 'D_USER' )) define ( 'D_USER', 'user' );
    
    // Définit pages Folder
    if (! defined ( 'D_PAGES' )) define ( 'D_PAGES', 'pages' );

    // Définit params Folder
    if (! defined ( 'D_PARAMS' )) define ( 'D_PARAMS', 'params' );

    // Définit users Folder
    if (! defined ( 'D_USERS' )) define ( 'D_USERS', 'users' );
    
    // Définit datas Folder
    if (! defined ( 'D_DATAS' )) define ( 'D_DATAS', 'datas' );
   
    // Définit core Folder
    if (! defined ( 'D_CORE' )) define ( 'D_CORE', 'core' );

    // Définit app Folder
    if (! defined ( 'D_APP' )) define ( 'D_APP', 'app' );

    // Définit mods modules Folder
    if (! defined ( 'D_MODS' )) define ( 'D_MODS', 'mods' );
    
    // Définit stores stores Folder
    if (! defined ( 'D_STORES' )) define ( 'D_STORES', 'stores' );    

    // Définit misc Folder
    if (! defined ( 'D_MISC' )) define ( 'D_MISC', 'misc' );
    
    // Définit help Folder
    if (! defined ( 'D_HELP' )) define ( 'D_HELP', 'help' );
        
    // Définit libs Folder
    if (! defined ( 'D_LIBS' )) define ( 'D_LIBS', 'libs' );
    
    // Définit le testing Folder (module de tests)
    if (! defined ( 'D_TESTING' )) define ( 'D_TESTING', 'testing' );
    
    // Définit le private Folder
    if (! defined ( 'D_PRIVATE' )) define ( 'D_PRIVATE', 'private' );

    // Définit l' exports Folder
    if (! defined ( 'D_EXPORTS' )) define ( 'D_EXPORTS', 'exports' );

    // Définit l' imports Folder
    if (! defined ( 'D_IMPORTS' )) define ( 'D_IMPORTS', 'imports' );
    
    // Définit includes Folder  (for inclusion snippet code)
    if (! defined ( 'D_INC' )) define ( 'D_INC', 'includes' );

    // Définit le tpl Folder
    if (! defined ( 'D_TPL' )) define ( 'D_TPL', 'tpl' );
    
    // Définit le functions Folder
    if (! defined ( 'D_FCT' )) define ( 'D_FCT', 'functions' );
    
    // Définit le classes Folder
    if (! defined ( 'D_CLAS' )) define ( 'D_CLAS', 'classes' );
       
    // Définit les constantes du mode du routeur
    if (! defined ( 'M_INCL' )) define ( 'M_INCL', 'INCL' );
    if (! defined ( 'M_REDIR' )) define ( 'M_REDIR', 'REDIR' );
    
    /* ++++++++++++++++++++++++++++++++++++++++ */
    /* +++    Constantes de l'Application   +++ */
    /* ++++++++++++++++++++++++++++++++++++++++ */
    
    // Définit le nom de l'application
    if(!defined('K_NAME')) define( 'K_NAME','CODE(s)');


     // Definit Numéro de version de l'application
    if(!defined('K_VER')) define( 'K_VER', ' 1.3.0');
    
    // Definit l'année du lancement du développement de l'app 
    if(!defined('K_YEAR')) define( 'K_YEAR', '2019');
   
    // Définit le titre générique des page de l'application
    if(!defined('K_TITLE')) define( 'K_TITLE',K_NAME.' '.K_VER); 
    
    // Auteur de l'application 
    if(!defined('K_AUTHOR')) define( 'K_AUTHOR','KTDev');

    // Site web de l'auteur de l'application 
    if(!defined('K_AUTHOR_SITE')) define( 'K_AUTHOR_SITE','https://www.ktdev.pro');
    
    // Site web de l'auteur de l'application 
    if(!defined('K_APP_SRC')) define( 'K_APP_SRC','https://git.ktdev.pro/KTDev/Codes/releases');

    // Description de l'application (SEO)
    if(!defined('K_DESCRIPTION')) define( 'K_DESCRIPTION', K_NAME.' - Code(s) by KTDev');

    // Keywords de l'application (SEO)
    if(!defined('K_KEYWORDS')) define( 'K_KEYWORDS','snippet, code source, kiss');   

    // REVISIT-AFTER de l'application (SEO)
    if(!defined('K_REVISIT_AFTER')) define( 'K_REVISIT_AFTER', '3 days');

    // Date de création de l'application (SEO)
    if(!defined('K_DATE_CREATION')) define( 'K_DATE_CREATION', '27/06/2019');

    // ROBOTS de l'application (SEO)
    if(!defined('K_ROBOTS')) define( 'K_ROBOTS', 'index,follow');   
    
    // Définit le thème utilisé par défaut
    if (! defined ( 'D_THM_USE' )) define ( 'D_THM_USE', 'darkmaul' ); 


    /* ++++++++++++++++++++++++++++++ */
    /* +++    Constantes  Thème   +++ */
    /* ++++++++++++++++++++++++++++++ */

    // Définit Bootstrap  Version
    if (! defined ( 'K_BOOTS_VER' )) define ( 'K_BOOTS_VER', '3.3.6' );

    // Définit Jquery  Version
    if (! defined ( 'K_JQUERY_VER' )) define ( 'K_JQUERY_VER', '1.12.0' );

    // Définit JqueryUI  Version
    if (! defined ( 'K_JQUERYUI_VER' )) define ( 'K_JQUERYUI_VER', '1.11.4' );
    
    // Définit JQwidget  Version
    if (! defined ( 'K_JQWIDGET_VER' )) define ( 'K_JQWIDGET_VER', '4.0.0' );

    // Définit Bootstrap  Folder
    if (! defined ( 'D_BOOTS' )) define ( 'D_BOOTS', 'bootstrap' );

    // Définit Themes Folder
    if (! defined ( 'D_THEMES' )) define ( 'D_THEMES', 'themes' );

    // Définit CSS Folder
    if (! defined ( 'D_CSS' )) define ( 'D_CSS', 'css' );

    // Définit JS Folder
    if (! defined ( 'D_JS' )) define ( 'D_JS', 'js' );
    
    // Définit images Folder
    if (! defined ( 'D_IMG' )) define ( 'D_IMG', 'images' );   
    
    // Définit le chemin et le  fichier index par défaut au lancement de l'application
    if (! defined ( 'K_DEF_INDEX' )) define ( 'K_DEF_INDEX', D_APP . DS . D_MODS . DS . D_HOME . DS . 'login' );
    
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    /* +++    Chargement des fichiers de classes et fonctions   +++ */
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    require_once D_CORE . DS . 'loading.inc.php';

