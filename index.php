<?php
//-----------------------------------------------------------------------------
    /**
     * Force l'affichage des erreurs si le mode debug est désactivé
     * Activer le mode debug est préférable dans le fichier:
     * private/conf.params.php
     */
        //require'__display__errors__.php';
//-----------------------------------------------------------------------------

define( 'KT_ROOT', getcwd() );
//define( 'DS', DIRECTORY_SEPARATOR );
define( 'DS', '/' );
try{
    // Chargement du bootstrap
    require KT_ROOT . DS . 'bootstrap.php';    

}catch(Exception $e){
    print_r($e);
}



