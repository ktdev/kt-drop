
--
-- Base de données: `code`
--

-- --------------------------------------------------------

--
-- Structure de la table `params`
--

CREATE TABLE IF NOT EXISTS `params` (
  `id_params` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) DEFAULT NULL,
  `value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_params`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `params`
--

INSERT INTO `params` (`id_params`, `key`, `value`) VALUES
(1, 'INSTALL', 'OK'),
(7, 'PATH_INSTALL', '/'),
(8, 'TXT_WELCOM', 'Votre introduction'),
(9, 'TXT_NOTE', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(25) NOT NULL,
  `email` varchar(60) NOT NULL,
  `hpasswd` varchar(255) DEFAULT NULL,
  `actif` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id_user`, `pseudo`, `email`, `hpasswd`, `actif`) VALUES
(1, 'demo', 'demo@demo.com', '$2y$10$jMvnvjMKwYzT5A0NvPOYHuPZ56z1uLFSoGxyBkx0m5qykNvtzUkN6', 1);

