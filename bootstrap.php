<?php

//-----------------------------------------------------------------------------
// Definit Bootstrap
if (! defined ( 'K_BOOTSTRAP' )) define ( 'K_BOOTSTRAP', 'if (!isset ( $_SESSION [\'bootstrap\'] )) $_SESSION [\'bootstrap\'] = TRUE;' );
// Definit ABSPATH comme répertoire racine du site
if (! defined ( 'ABSPATH' )) define ( 'ABSPATH', dirname ( __FILE__ ) . DS );
//-----------------------------------------------------------------------------

/* 
    Chargement des fichiers de configuration, celui de l'utilisateur de l'application
    et celui de la configuration de base de l'application 
 */
require_once 'private' . DS . 'params' . DS . 'user.params.php';
require_once 'conf.app.php';

// Initialisation du bootstrap
eval( K_BOOTSTRAP );

// Vérification de l'installation
(KTInstaller( 'CTRL_INSTALL' )) ? $_SESSION['startAndStop'] = true : $_SESSION['startAndStop'] = false;

// Lancement de l'application
if( $_SESSION['startAndStop'])
{
    try{

        // Chargement de chemin d'installation de l'application
        $dir = KT_ROOT.DS.D_PRIVATE.DS.D_PARAMS.DS;
        $pathInstall = KTloadContentFile($dir, 'path-install.php');

        // Globalisation du chemin d'installation
        $G_PATH_INSTALL = $pathInstall;

        // Chargement et globalisation du texte d'introduction et de la note       
        $G_TXT_WELCOM = KTloadContentFile($dir, 'text-intro.php');
        $G_TXT_NOTE = null;

        // Initialiser une constante avec le pathInstall
        if (! defined ( 'K_PTH_INST' )) define ( 'K_PTH_INST', $pathInstall );
        // Créer les chemins d'accès vers les modules
        if (! defined ( 'K_PTH_MODS' )) define ( 'K_PTH_MODS', K_PTH_INST.D_APP.DS.D_MODS.DS);
        // Créer les chemins d'accès vers le répertoire themes
        if (! defined ( 'K_PTH_THM' )) define ( 'K_PTH_THM', D_THEMES.DS );
        // Créer les chemins d'accès vers le Thème en cours
        if (! defined ( 'K_PTH_THM_USE' )) define ( 'K_PTH_THM_USE', K_PTH_INST.D_THEMES.DS.D_THM_USE.DS );

        // Définir la requête(path) et les paramètres(query)
        $pathAndQuery = getPathAndQueryFromUrl($pathInstall, $_SERVER ['REQUEST_URI']);
              
        // Si l'uri correspond à la racine du site on demande d'afficher le module/page par défaut 
        if(empty($pathAndQuery['path']) || $pathAndQuery['path'] == '/'){
            $GLOBALS['URI_ROOT'] = true;  
            $route = new Router(K_DEF_INDEX);          
            $GLOBALS['G_ROUTER'] = $route->KTRouter( M_INCL, $pathAndQuery['query'] );
            //Sinon on demande au router d'inclure la page
        }else{
            $GLOBALS['URI_ROOT'] = false;
            $route = new Router($pathAndQuery['path']);   
            $GLOBALS['G_ROUTER'] = $route->KTRouter( M_INCL,  $pathAndQuery['query'] );
        }
    }catch(Exception $e){
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
    }
   
}else{
    echo "Veuillez lancer l'installation de l'application";
}

