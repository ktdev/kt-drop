<?php
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ ++

// ********************************* CONTROLE DE L'ACCES *********************************
// ***************************************************************************************

// Module en cours
$pathAndQuery = null;
$pathAndQuery = getPathAndQueryFromUrl(K_PTH_INST, $_SERVER ['REQUEST_URI']);
$route = new Router($pathAndQuery['path']);
$currentModule = $route->getCurrentModule();

// Vérification de l'accès au module
$users = new UsersCodes();
$access = $users->KTgetAccessModule($currentModule, $_SESSION['role']);

// DROITS AUTORISES : |NO| = 0 / |READ| = 1 / |WRITE| = 2 / |FULL| = 3 
if($access < 3)
{   
   header('Refresh: 0; url= '.K_PTH_INST . D_APP . DS . D_MODS . DS . D_HOME . DS . 'noaccess'); 
   die();
}
// ***************************************************************************************

// Initialisations
$login = null;
$email = null;
$password = null;
$passwordConfirm = null;
$role = null;
$status = null;
$message = null;
$selectedRole = null;
$selectedStatus = null;
$hiddenPassword = null;
$creeruser = 1;

// ******************************** LISTER LES UTILISATEURS ******************************
// ***************************************************************************************

$usersList = $users->KTgetUsers();
$tableUsers = $users->KTtableCodesUsers($usersList);

// ********************************* GESTION DES POST ************************************
// ***************************************************************************************
if(!empty($_POST))
{
  
   $stopProcess = false;
   $passwordExigences = true;
    
   // ********************************* AJOUTER UN UTILISATEUR ******************************
   // ***************************************************************************************
   
   if(isset($_POST['creeruser']))
   {
      
      //DEBUG//echo DEBUG::KTprint_r($_POST);
      $creeruser = $_POST['creeruser'];

      // -----------------------------------------------------------------------------------------------
      // CONTROLE DES ENTREES
      // -----------------------------------------------------------------------------------------------
      
      // Contrôle du login -----------------------------------------------------------------------------
      if(!empty($_POST['login']))
      {
         // Uniquement des caractères aplhanumériques
         $stCtypeAlNum = ctype_alnum ($_POST['login']);
         if($stCtypeAlNum)
         {
            $login = $_POST['login'];
         }else{
            $stopProcess = true;
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("Le login ne peut contenir que des caractères alphanumériques").' (aA-zZ, 0-9)', 'alert'); 
         }             
      }else{
         $stopProcess = true;
         $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("Le login est obligatoire"), 'alert'); 
      }

      // Contrôle de l'e-mail --------------------------------------------------------------------------
      if(!empty($_POST['email']) && $stopProcess == false)
      {
         // Vérifier validité email
         $stFilterVarEmail = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL); 
         if($stFilterVarEmail)
         {
            $email = $_POST['email'];
         }else{
            $stopProcess = true;
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("L'adresse e-mail n'est pas conforme"), 'alert'); 
         }       
      }else{
         if($stopProcess == false)
         {
            $stopProcess = true;
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("L'adresse e-mail est obligatoire"), 'alert'); 
         }        
      }

      // Contrôle du mot de passe -----------------------------------------------------------------------
      if(!empty($_POST['password']) && $stopProcess == false)
      {
         
         $stValidPassword = KTvalidPassword($_POST['password'], 8);
         if($stValidPassword)
         {
            $password = $_POST['password'];  
         }else{
            $stopProcess = true;
            $passwordExigences = false;           
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("ATTENTION, le mot de passe n'est pas conforme aux exigences"), 'alert'); 
         }    

      }else{

         // Si le mot de passe est vide et que l'on édite un user 
         if($creeruser == 0)
         {
            // Si le mot de passe caché est initialisé
            if(!empty($_POST['hiddenpasswd']))
            {
               $password = $_POST['hiddenpasswd'];              
            }
         }elseif($stopProcess == false)
         {
            $stopProcess = true;
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("Le mot de passe est obligatoire"), 'alert'); 
         } 
         
         
      }

      // Contrôle du mot de passe de confirmation -------------------------------------------------------
      if(!empty($_POST['passwordConfirm']) && $stopProcess == false)
      {
            // On vérifie uniquement la concordance des mots de passe
            if ($_POST['passwordConfirm'] != $password)
            {
               $stopProcess = true;
               $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("Les mots de passe ne correspondent pas"), 'alert'); 
            }else{
               // Si tout va bien, on hash le mot de passe et on initalise $password avec
               $hashPassword = KTHashPasswd($password);
               if($hashPassword != false)
                  $password = $hashPassword;            
            }

      }else{
         
         // Si on crée un user
         if($creeruser == 1)
         {

            if($stopProcess == false)
            {
               $stopProcess = true;

               if(!$passwordExigences)
                  $message = "ATTENTION, le mot de passe n'est pas conforme aux exigences";
               else
                  $message = T_("La confirmation du mot de passe est obligatoire");

               $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $message, 'alert'); 
            }
         }

      }

      // Contrôle du rôle -------------------------------------------------------------------------------
      if(!empty($_POST['roles']) || $stopProcess == false)
      {
         $role = $_POST['roles'];  
      }

      // Contrôle du status -----------------------------------------------------------------------------
      if(!empty($_POST['status']) || $stopProcess == false)
      {
         $status = $_POST['status'];  
      }

      // ------------------------------------------------------------------------------------------------
      // Si tout est OK on enregistre les données dans un fichier
      // ------------------------------------------------------------------------------------------------
      if(!$stopProcess)
      {
         
         // Création d'un tableau comprenant les données de l'utilisateur
         $userInfos = array();

         $userInfos['LOGIN'] = $login;
         $userInfos['EMAIL'] = $email;
         $userInfos['PASSWORD'] = $password;
         $userInfos['ROLE'] = $role;
         $userInfos['STATUS'] = $status;     
         
         $userArray['parameters'] = $userInfos;

         //DEBUG//echo debug::KTprint_r($userArray);

         // Tentative de création du fichier de paramètres
         $dirParamsUsers = KT_ROOT.DS.D_PRIVATE.DS.D_USERS.DS.strtoupper($login).'.ini';
         $objIniUser = new ini ($dirParamsUsers);   
         
         // Si on crée un nouvel utilisateur
         if($creeruser == 1)   
         {        
            $objIniUser->ajouter_array($userArray);
            $st = $objIniUser->ecrire();
         // Si on modifie un utilisateur on force la réécriture avec (true)   
         }elseif($creeruser == 0){
            $objIniUser->ajouter_array($userArray);
            $st = $objIniUser->ecrire(true);           
         }

         // Gestion des messages en fonction du résultat
         if($st['stat'] === true)
         {
            if($creeruser == 1){
               $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("Un nouvel utilisateur a été créé"), 'success');  
            }elseif($creeruser == 0){
               $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("L'utilisateur a été modifié"), 'success');  
            }
         }else{
            if($creeruser == 1){
               $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("L'enregistrement du nouvel utilisateur a échoué"), 'alert');               
            }elseif($creeruser == 0){
               $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("La mise à jour de l'utilisateur a échoué"), 'alert');               
            }
            
         }

         //DEBUG//echo debug::KTprint_r($userArray);

         // Tout c'est bien passé, on vide les variables du formulaire
         $login = null;
         $email = null;
         $password = null;
         $passwordConfirm = null;

          // Raffraîchissement de la page après 1 seconde
         header('Refresh: '.K_REFRESH.'; url= gestion-users'); 
         
      }    

     
     
   } // END CREERUSER

   
  
} // END $_POST



// ********************************** GESTION DES GET ************************************
// ***************************************************************************************

if(!empty($_GET))
{
   // ***************************** EDIT USER *********************************
   // *************************************************************************
   if($_GET['act'] == 'edit')
   {
      // Chargement des données dans le formulaire
      $loginGet = $_GET['login'];
      $user = $users->KTLoadUser(strtoupper($loginGet).'.ini');
      $userInfos = $users->KTgetUser();
      $login = $userInfos['LOGIN'];
      $email = $userInfos['EMAIL'];
      $selectedRole = $userInfos['ROLE'];
      $selectedStatus = $userInfos['STATUS'];
      $hiddenPassword = $userInfos['PASSWORD'];
      $creeruser = 0;
      
      //DEBUG//echo DEBUG::KTprint_r($userInfos);

   // *************************** DELETE USER *********************************
   // *************************************************************************
   }elseif($_GET['act'] == 'del')
   {

      $loginGet = $_GET['login'];
      $userToDelete = KT_ROOT.DS.D_PRIVATE.DS.D_USERS.DS.strtoupper($loginGet).'.ini';
      
      if(@unlink($userToDelete)){
         $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("L'utilisateur a été supprimé"), 'success');  
      }else{
         $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("La suppression de l'utilisateur a échoué"), 'alert');               
      }

      // Raffraîchissement de la page après 1 seconde
      header('Refresh: '.K_REFRESH.'; url= gestion-users'); 
      
   }
  
}


//echo DEBUG::KTprint_r($roles);

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_ADM . DS );

// Assignation du template
$engine->set_file( D_USER, 'tpl_gestion-users.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Variables et termes à afficher
$engine->set_var('titre-gestion-users', T_('Gestion des utilisateurs'));  

// On place en readonly l'input login lors de l'édition
if(!empty($_GET) && $_GET['act'] == 'edit')
{
   $strHtmlInputLogin = '<span class="input-group-addon">{trm-login} *</span><input type="text" class="form-control" id="login" name="login" readonly value="{value-login}" >';
   $engine->set_var('input-login', $strHtmlInputLogin);  
}else{
   $strHtmlInputLogin = '<span class="input-group-addon">{trm-login} *</span><input type="text" class="form-control" id="login" name="login" value="{value-login}" >';
   $engine->set_var('input-login', $strHtmlInputLogin);
}


$engine->set_var('value-login', $login);  
$engine->set_var('value-email', $email);  
$engine->set_var('value-password', $password);  
$engine->set_var('table-users', $tableUsers);  
$engine->set_var('select-roles', $users->KTSelectRoles($selectedRole));  
$engine->set_var('select-status', $users->KTSelectStatus($selectedStatus));  
$engine->set_var('hidden-password', $hiddenPassword);  
$engine->set_var('creer-user-value', $creeruser); 
$engine->set_var('trm-explications-password', T_('Lorsque que vous créé un nouvel utilisateur le mot de passe (et sa confirmation) 
sont obligatoires. Quand vous éditez un utilisateur le mot de passe et sa confirmation sont facultatifs ne remplissez 
ses deux champs que si vous souhaitez modifier le mot de passe actuel de l\'utilisateur')); 
$engine->set_var('modale-delete', displayModalDelete('{trm-supprimer-user}','{trm-supprimer-user-confirm}')); 
$engine->set_var('refresh-users', '<a class="btn btn-success btn-xs" href="{url-admin-gestion-users}"><i class="fa fa-undo "></i> '.T_("Réinitialiser").'</a>'); 


// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
  
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_USER );

// Rendu du template
$engine->p( 'display' );
