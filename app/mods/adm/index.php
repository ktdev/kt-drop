<?php

//----------------------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++   

// ********************************* CONTROLE DE L'ACCES *********************************
// ***************************************************************************************
// Module en cours
$pathAndQuery = null;
$pathAndQuery = getPathAndQueryFromUrl(K_PTH_INST, $_SERVER ['REQUEST_URI']);
$route = new Router($pathAndQuery['path']);
$currentModule = $route->getCurrentModule();

// Vérification de l'accès au module
$users = new UsersCodes();
$access = $users->KTgetAccessModule($currentModule, $_SESSION['role']);

// DROITS AUTORISES : |NO| = 0 / |READ| = 1 / |WRITE| = 2 / |FULL| = 3 
if($access < 3)
{
   header('Refresh: 0; url= '.K_PTH_INST . D_APP . DS . D_MODS . DS . D_HOME . DS . 'noaccess'); 
   die();
}

// ************************************** MESSAGE ****************************************
// ***************************************************************************************
// Si passage d'un message par l'url
if(!empty($_GET['msg']))
   $msg = $_GET['msg'];

// ************************************** CHECK OS ***************************************
// ***************************************************************************************
// Import / Export uniquement sous Linux
if(PHP_OS == 'Linux')
{
   $flagOS = '<i class="fa fa-check-square flagSquare" title="Fonctionnalité disponible"></i>';
   $status = '';
}else {
   $flagOS = '<i class="fa fa-minus-circle flagMinus" title="Fonctionnalité non disponible"></i>';
   $status = 'disabled';
}

   // *******************  Si l'action est un Export ****************** 
   // ***************************************************************** 
   if(!empty($_POST['action']) && $_POST['action'] == 'export')
   {
      $out = NULL;
      $filename = 'KTCodes-datas-'.date("dmy-His").'.tar.gz';
      $pathFile = KT_ROOT.DS.D_PRIVATE.DS.D_EXPORTS.DS;
      $dirToExport = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS;

      $cmd ='tar czf '.$pathFile.$filename.' -C '.KT_ROOT.DS.D_PRIVATE.DS.' '.D_DATAS;
   
      exec($cmd, $op, $retrn); 
      
      if(file_exists($pathFile.$filename)){
         header("Content-Type: application/force-download");
         header("Content-Transfer-Encoding: application/x-gzip"); 
         header("Content-Disposition: attachment; filename=".$filename);
         header("Pragma: no-cache");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
         header("Expires: 0"); 
         ob_clean();
         flush();
         readfile($pathFile.$filename);
         exit;
      }else{ 
            $txt = T_("Une erreure s'est produite lors de l'export");
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert'); 
      }
   }

   // *******************  Si l'action est un Import ****************** 
   // ***************************************************************** 
   if(!empty($_POST['action']) && $_POST['action'] == 'import')
   {
          
      if ($_FILES['importFile']['error'])
      {     
         switch ($_FILES['importFile']['error'])
         {     
            case 1: // UPLOAD_ERR_INI_SIZE
               $txt = T_('Le fichier dépasse la limite autorisée par le serveur');      
               $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert'); 
               break;     
            case 2: // UPLOAD_ERR_FORM_SIZE
                  $txt = T_('Le fichier dépasse la limite autorisée dans le formulaire'); 
                  $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert');      
               break;     
            case 3: // UPLOAD_ERR_PARTIAL
                  $txt = T_('L\'envoi du fichier a été interrompu pendant le transfert'); 
                  $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert');     
               break;     
            case 4: // UPLOAD_ERR_NO_FILE
                  $txt = T_('Le fichier que vous avez envoyé a une taille nulle'); 
                  $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert');     
               break;     
         } 

      }else{

         //Si il n'y a eu aucune erreur     
         if ($_FILES['importFile']['error'] == 0)
         { 

            // Initialisations
            $nomFileImport = $_FILES['importFile']['name'];
            $pathFileImport = pathinfo($nomFileImport);
            $extensionFileImport = $pathFileImport['extension'];
            $extensionsAutorisee = 'gz';
            $pathImport = KT_ROOT.DS.D_PRIVATE.DS.D_IMPORTS.DS;
            $datasDirectory = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS;

            // Si l'extension n'est pas correcte
            if ($extensionFileImport != $extensionsAutorisee)
            {
               $txt = T_("L'extension du fichier d'importation doit être: tar.gz");
               $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert'); 
            }else{ 

               $repertoireDestination = $pathImport;
               $filename = 'KTCodes-datas-'.date("dmy-His").'.tar.'.$extensionFileImport;

               // Copie du fichier dans le répertoire d'importation
               if (move_uploaded_file($_FILES["importFile"]["tmp_name"], $repertoireDestination.$filename))
               {

                     $retrn = null;
                     $els = false;

                     // Tentative de suppression du répertoire                   
                     if(file_exists (KT_ROOT.DS.D_PRIVATE.DS.D_DATAS))
                        $retrn = KTdeleteDirectory(KT_ROOT.DS.D_PRIVATE.DS.D_DATAS);
                     else
                        $retrn = true;
                        
                     // Si la suppression du répertoire a été effectuée
                     if($retrn){
                        // Tentative de copie du fichier d'import vers le répertoire private
                        $retrn = copy(KT_ROOT.DS.D_PRIVATE.DS.D_IMPORTS.DS.$filename, KT_ROOT.DS.D_PRIVATE.DS.$filename);
                     }else{
                        $txt = T_('La suppression du répertoire a échouée');    
                        $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert');                    
                        $els = true;
                     }

                     // Si la tentative de copie du fichier d'import vers le répertoire private est réussie
                     if($retrn){
                        // Tentative de désarchivage du fichier d'import
                        $retrn = null;
                        $els = false;
                        chdir(KT_ROOT.DS.D_PRIVATE.DS);
                        $cmdTar ='tar -xzf '.$filename;
                        $retrn = exec($cmdTar, $opTar, $retrnTar);                        
                        if(!$retrnTar) $retrn = true;
                     }elseif(!$els){
                        $txt = T_("La tentative de copie du fichier d'import a échouée");   
                        $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert'); 
                        $els = true;
                     }

                     // Si la tentative de désarchivage du fichier d'import est réussie
                     if($retrn){
                        // Tentative de suppression du fichier d'import
                        $retrn = null;
                        $els = false;
                        $cmd = 'rm -f '.KT_ROOT.DS.D_PRIVATE.DS.$filename;
                        exec($cmd, $op, $retrn); 
                        if($retrn == 0) $retrn = true;
                     }elseif(!$els){
                        $txt = T_("La tentative de désarchivage du fichier d'import a échouée");  
                        $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert'); 
                        $els = true;
                     }
                     
                     // Si la tentative de suppression du fichier d'import est réussie
                     if($retrn){
                        $txt = T_("L'importation s'est déroulée avec succès");
                        $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', $txt, 'success');                        
                        header('Location:'.K_PTH_INST . D_APP . DS . D_MODS . DS . D_ADM . DS . 'index?msg='.$msg); 
                     }elseif(!$els){
                        $txt = T_("La tentative d'import a échouée");   
                        $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert');
                     }

               }else{
                  $txt = T_("Le système a rencontré un problème lors de la copie du fichier dans le répertoire d'importation, vérifiez les droits et le propriétaire du répertoire");
                  $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $txt, 'alert'); 
               }
            }
         }     
      }     

   }
// ***************************************************************************************
// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_ADM . DS );

// Assignation du template
$engine->set_file( D_ADM, 'tpl_index.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Variables et termes à afficher
$engine->set_var('value-nbr-objets', countNbPages());
$engine->set_var('titre-administration', T_('Tableau de bord'));
$engine->set_var('trm-titre-module', T_('Gestion des snippets'));
$engine->set_var('trm-titre-import-export', T_('Importer/Exporter les données'));
$engine->set_var('trm-ajouter-objet', T_('Créer un nouveau snippet'));
$engine->set_var('trm-lister-gerer-objets', T_('Gérer les snippets'));
$engine->set_var('trm-exporter-datas', T_('Exporter les données'));
$engine->set_var('trm-importer-datas', T_('Importer les données'));
$engine->set_var('trm-linux-only', T_('Serveur Linux uniquement'));
$engine->set_var('status', $status);
$engine->set_var('flagOs', $flagOS);


// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_ADM );

// Rendu du template
$engine->p( 'display' );

