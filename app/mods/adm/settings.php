<?php

//-----------------------------------------------------------------------------

// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++   

// ********************************* CONTROLE DE L'ACCES *********************************
// ***************************************************************************************

// Module en cours
$pathAndQuery = null;
$pathAndQuery = getPathAndQueryFromUrl(K_PTH_INST, $_SERVER ['REQUEST_URI']);
$route = new Router($pathAndQuery['path']);
$currentModule = $route->getCurrentModule();

// Vérification de l'accès au module
$users = new UsersCodes();
$access = $users->KTgetAccessModule($currentModule, $_SESSION['role']);

// DROITS AUTORISES : |NO| = 0 / |READ| = 1 / |WRITE| = 2 / |FULL| = 3 
if($access < 3)
{
   header('Refresh: 0; url= '.K_PTH_INST . D_APP . DS . D_MODS . DS . D_HOME . DS . 'noaccess'); 
   die();
}

// ***************************************************************************************

$dirParams = KT_ROOT.DS.D_PRIVATE.DS.D_PARAMS.DS;
$textIntro = KTloadContentFile($dirParams, 'text-intro.php');
$pathInstall = KTloadContentFile($dirParams, 'path-install.php');
$costAppValue = KTloadContentFile($dirParams, 'cost.php');

// Calcul du cos Server ---------------------------------------------------------
$timeTarget = 0.05; // 50 millisecondes

$cost = 8;
do {
    $cost++;
    $start = microtime(true);
    password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
    $end = microtime(true);
} while (($end - $start) < $timeTarget);

$costServerValue = $cost;
//-------------------------------------------------------------------------------

if(!empty($_POST)) // Si le formulaire a été validé et qu'il n'est pas vide
{
    // Traitement du texte d'introduction -------------------------------------
    $textIntro = htmlspecialchars($_POST['textIntro']); 
    $stTextintro = file_put_contents($dirParams.'text-intro.php', $textIntro);    
    //--------------------------------------------------------------------------  
    
    // Traitement du de la valuer de cost  -------------------------------------
    $costAppValue = intval($_POST['costAppValue']);
    if(is_int($costAppValue)){
        $costAppValue = htmlspecialchars($_POST['costAppValue']); 
    }else{
        $costAppValue = $costServerValue;
    }

    $stCostApp = file_put_contents($dirParams.'cost.php', $costAppValue); 


    // Gestion des messages ----------------------------------------------------
    if($stTextintro &&  $stCostApp){
        $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("Les modifcations ont été validées"), 'success');               
    }else{
        $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("L'application a rencontré un problème lors de l'enregistrement"), 'alert');                      
    }
    //--------------------------------------------------------------------------  
    


}else{
    if(empty($textIntro))
        $textIntro = T_('Votre intro');

    if(empty($costAppValue))
        $costAppValue = $costServerValue;
}



// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_ADM . DS );

// Assignation du template
$engine->set_file( D_ADM, 'tpl_settings.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Variables et termes à afficher
$engine->set_var('titre-parametre', T_('Paramètres'));
$engine->set_var('trm-path-install', T_('Chemin d\'installation'));
$engine->set_var('trm-cost-explications', T_('Le cost détermine le coût algorithmique idéal pour créer la clé de hachage des mots de passe. 
    Si omis, la valeur par défaut 10 sera utilisée. 8-10 est une bonne base, mais une valeur plus élevée est aussi un bon choix à partir
    du moment où votre serveur est suffisament rapide '));
$engine->set_var('trm-cost-server-value', T_('Cost idéal du serveur'));
$engine->set_var('trm-cost-app-value', T_('Cost de l\'application'));
$engine->set_var('value-textIntro', $textIntro);
$engine->set_var('value-pathInstall', $pathInstall);
$engine->set_var('value-costServerValue', $costServerValue);
$engine->set_var('value-costAppValue', $costAppValue);
$engine->set_var('nom-fichier', D_PRIVATE.DS.D_PARAMS.DS.'path-install.php');



// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_ADM );

// Rendu du template
$engine->p( 'display' );

