<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    
    $createResult = null;
    $pageTitre = null;
    $pageContenu = null;
    $pageComments = null;
    $pageAuthor = null;
    $pageLastUpdate = null;
    $pageTagID = null;
    
    if(isset($_GET['id']))
        $pageID = $_GET['id'];
    
    if(!empty($_POST['titre']) && !empty($_POST['contenu'])) // Si le formulaire a été validé et qu'il n'est pas vide
    {
        $titre = htmlspecialchars($_POST['titre']); // La variable contenant le titre
        $comments = htmlspecialchars($_POST['comments']); // La variable contenant le titre
        //$contenu = nl2br(htmlspecialchars($_POST['contenu'])); // Le contenu
        $contenu = $_POST['contenu']; // Le contenu
        
        $titreSlug = KTslugify($titre);
        $pageID = $_POST['id-page'];
        $author = trim($_POST['codeauthor']);
        $lastupdate = trim($_POST['lastupdate']);
        $tag = $_POST['tags']; // L'étiquette'
        //DEBUG//echo 'pageID : '. $pageID; 
        
        // Edition de la page    
        $createResult = editPage($pageID, $titre, $comments, $contenu, $titreSlug, $author, $lastupdate, $tag);   

        if($createResult){
            $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("Les modifcations ont été validées <br> {lien-visionner}"), 'success');               
            $pageID = $titreSlug;
            
            // Mise à jour des données
            $donnees = viewPage($pageID);
            $pageTitre = $donnees['Titre'];
            $pageComments = $donnees['Comments'];
            $pageContenu = $donnees['Contenu'];
            $pageAuthor = trim($donnees['Author']);
            $pageLastUpdate = trim($donnees['LastUpdate']);
            $pageTagID = trim($donnees['TagID']);
        }else{
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("L'application a rencontré un problème lors de la modification du snippet"), 'alert');                      
        }
    }else{
        
        $pageTitre = null;
        $pageComments = null;
        $pageContenu = null;
        $pageAuthor = null;
        $pageLastUpdate = null;
        $pageTagID = null;

        if(empty($_GET['id'])) // Si on n'a pas fourni en paramètre l'id de la page à voir
         {
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("Vous devez fournir l'id de la page"), 'alert');     
         }else{
            $id =  $_GET['id'];
            $donnees = viewPage($id);
            $pageTitre = $donnees['Titre'];
            $pageComments = $donnees['Comments'];
            $pageContenu = $donnees['Contenu'];
            $pageAuthor = trim($donnees['Author']);
            $pageLastUpdate = trim($donnees['LastUpdate']);
            $pageTagID = trim($donnees['TagID']);
         }
    }    
    
// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_edit.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

$engine->set_var('head_section_addons', insertHeadSection(buildLink(K_PTH_THM)));
$engine->set_var('footer-js-section-addons', insertFooterJsSection(buildLink(K_PTH_THM)));

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);

// Variables générales
$engine->set_var('titre-page', T_('Modifier le snippet'));
$engine->set_var('creer-snippet', '<a class="btn btn-success btn-xs" href="{url-primary-creer}">'.T_("Créer un snippet").'</a>');
$engine->set_var('list-snippet', '<a class="btn btn-primary btn-xs" href="{url-primary-index}">'.T_("Accueil").'</a>');
$engine->set_var('lien-visionner','<a class="wBlueColor" href="{url-primary-page}?id='.$pageID.'">'.T_("Voir le snippet").'</a>');
$engine->set_var('restaurer-snippet', '<a class="btn btn-success btn-xs" href="{url-primary-restore}?id='.$pageID.'">'.T_("Restaurer une version").'</a>');
$engine->set_var('gestion-snippets', '<a class="btn btn-success btn-xs" href="{url-primary-adm-pages}"><i class="fa fa-cog "></i> '.T_("Gérer").'</a>');

$engine->set_var('value-titre', $pageTitre);
$engine->set_var('value-contenu', $pageContenu);
$engine->set_var('value-comments', $pageComments);
$engine->set_var('id-page', $pageID);
$engine->set_var('codeauthor', $pageAuthor);
$engine->set_var('lastupdate', $pageLastUpdate);

$tagsList = KTLoadTagsList();
$engine->set_var('select-tags', KTSelectTags($tagsList, $pageTagID));  

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{

} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );


