
<?php

//----------------------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

// ********************************* CONTROLE DE L'ACCES *********************************
// ***************************************************************************************

// Module en cours
$pathAndQuery = null;
$pathAndQuery = getPathAndQueryFromUrl(K_PTH_INST, $_SERVER ['REQUEST_URI']);
$route = new Router($pathAndQuery['path']);
$currentModule = $route->getCurrentModule();

// Vérification de l'accès au module
$users = new UsersCodes();
$access = $users->KTgetAccessModule($currentModule, $_SESSION['role']);

// DROITS AUTORISES : |NO| = 0 / |READ| = 1 / |WRITE| = 2 / |FULL| = 3 

if($access < 2)
{
    header('Refresh: 0; url= '.K_PTH_INST . D_APP . DS . D_MODS . DS . D_HOME . DS . 'noaccess'); 
    die();
}
  

// ***************************************************************************************

if(!empty($_GET['id'])) { 
    $id = $_GET['id']; 
    $datas = viewPage($id);
}

$versionsListe = NULL;
$btnRetourPageConsultee = NULL;
$previsualisation = NULL;
$titreSlug = NULL;

$repertoire = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$id;

    // Lister les sauvegardes
    // 
    // On liste les dossiers du répertoire de sauvegarde   
    $liste_dossiers = glob($repertoire.DS.'save'.DS.'*'); 
    
    // Tri dates décroissantes
    arsort($liste_dossiers);

    $versionsListe = '<div class="table-responsive mTop20"><table class="table table-hover table-condensed">';
    $versionsListe .= '<thead><tr><th>O</th><th>Restore</th><th>Del</th></tr></thead><tbody>';      
   
    // On explore le tableau qui contient la liste des dossiers
    foreach($liste_dossiers as $key) 
    {
        // On vérifie qu'il s'agit bien d'un dossier, car glob retourne fichiers et dossiers
        if(is_dir($key)) 
        {
            $rep = $repertoire.DS.'save'.DS;
            // On récupère le timestamp(càd le nom du dossier)
            $timestamp = explode(DS, $key);
            $indexTimestamp = count($timestamp)-1;
            // $timestamp[x] correspond au timestamp (nom du dossier)
            $date = date('d/m/Y', $timestamp[$indexTimestamp]); 
            // On stock l'heure
            $heure = date('h\Hi\m\ns\s', $timestamp[$indexTimestamp]);

            $dir = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$id.DS.'save'.DS.$timestamp[$indexTimestamp];

            if(file_exists($dir.DS.'titre.txt'))
                $titre = file_get_contents($dir.DS.'titre.txt');
            if(file_exists($dir.DS.'comments.txt'))
                $comments = file_get_contents($dir.DS.'comments.txt');
            if(file_exists($dir.DS.'contenu.txt'))
                $contenu = file_get_contents($dir.DS.'contenu.txt');
            
             
             // On affiche un lien (pour voir la copie) avec l'heure et la date de sa création                  
            $versionsListe .= '<tr>
                                <td><i class="fa fa-caret-square-o-right eBlue" aria-hidden="true"></i></td>
                                <td><a href="restore?id='.$id.'&view='.$timestamp[$indexTimestamp].'&date='.$date.' - '.$heure.'#prev">'.T_("Version du :").' '.$date.' à  '.$heure.'</a></td>
                                <td><a href="restore?id='.$id.'&del='.$timestamp[$indexTimestamp].'"><i class="fa fa-trash el_error" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="'.T_("Supprimer ce point de restauration").'"></i></a></td>
                            </tr>';
                       
        }
    }

    $versionsListe .= '</tbody></table></div>';
    
    
    // Afficher le contenu de la restauration
    if(isset($_GET['view']))
    { 
        if(file_exists($repertoire.DS.'save'.DS.intval($_GET['view']).DS.'titre.txt'))
            $lecture_titre = file_get_contents($repertoire.DS.'save'.DS.intval($_GET['view']).DS.'titre.txt');
        if(file_exists($repertoire.DS.'save'.DS.intval($_GET['view']).DS.'comments.txt'))
            $lecture_comments = file_get_contents($repertoire.DS.'save'.DS.intval($_GET['view']).DS.'comments.txt');
        if(file_exists($repertoire.DS.'save'.DS.intval($_GET['view']).DS.'contenu.txt'))
            $lecture_contenu = html_entity_decode(file_get_contents($repertoire.DS.'save'.DS.intval($_GET['view']).DS.'contenu.txt'));
        $date = $_GET['date'];

        $previsualisation =
        '<div id="prev" class="container-prev">'.    
        '<div class="row b_row">'.
        '<div class="col-md-10 b_restore">'.
        '<a class="" href="restore?id='.$id.'&up='.$_GET['view'].'">'.T_("Restaurer la version du : ").'<span class="date_high">'.$date.'</span></a> '.
        '</div>'. 
        '<div class="col-md-2 b_fermer">'.
        '<a class="" href="restore?id='.$id.'"><i class="fa fa-times" aria-hidden="true"></i> '.T_(" Fermer").'</a>'.
        '</div></div>'.   

        '<div class="container-content">'. 
        '<label class="lblPrev">'.T_("Titre").'</label> '.    
        '<div class="prevTitre">'.$lecture_titre.'</div>'.    
        '<label class="lblPrev">'.T_("Commentaires").'</label> '.    
        '<div class="prevTitre">'.$lecture_comments.'</div>'. 
        '<label class="lblPrev">'.T_("Contenu").'</label> '. 
        '<div class="prevContenu"><textarea class="form-control codemirror-textarea" name="contenu" id="contenu">'.$lecture_contenu.'</textarea></div>'. 
        '</div>'.

        '<div class="row b_row">'.
        '<div class="col-md-10 b_restore">'.
        '<a class="" href="restore?id='.$id.'&up='.$_GET['view'].'">'.T_("Restaurer la version du : ").'<span class="date_high">'.$date.'</span></a> '.
        '</div>'. 
        '<div class="col-md-2 b_fermer">'.
        '<a class="" href="restore?id='.$id.'"><i class="fa fa-times" aria-hidden="true"></i> '.T_(" Fermer").'</a>'.
        '</div></div>'.    
        '</div>';

    }
    
    // Réception de la demande de restauration et traitement de celle-ci
    if(isset($_GET['up']))
    { 
        $up = intval($_GET['up']); 
        $repertoireOld = $repertoire;
        
        $titreSlug = file_get_contents($repertoire.DS.'save'.DS.$up.DS.'titre.txt');
        $titreSlug = KTslugify($titreSlug);
        $repertoireNew = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$titreSlug;
               
        
        // Copie de l'ancien répertoire
        $stCopyDir = KTcopyDirectory($repertoireOld, $repertoireNew);
        
        //On récupère le timestamp de la copie que l'on veut remettre en ligne    
        $copy = copy($repertoireNew.DS.'save'.DS.$up.DS.'titre.txt', $repertoireNew.DS.'titre.txt'); // On remplace le titre actuel par celui de la copie
        $copy = copy($repertoireNew.DS.'save'.DS.$up.DS.'comments.txt', $repertoireNew.DS.'comments.txt'); // On remplace le commentaire actuel par celui de la copie
        $copy = copy($repertoireNew.DS.'save'.DS.$up.DS.'contenu.txt', $repertoireNew.DS.'contenu.txt'); // On remplace le contenu actuel par celui de la copie
        $copy = copy($repertoireNew.DS.'save'.DS.$up.DS.'infos.ini', $repertoireNew.DS.'infos.ini'); // On remplace le fichier infos actuel par celui de la copie
        $id = $titreSlug;

        // Suppression de l'ancien répertoire
        if(file_exists($repertoireOld))
            $stDelDir = KTdeleteDirectory($repertoireOld);
                           
        $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("La version sélectionnéé a été restaurée").'<br>'.'<a class="wBlueColor" href="page?id='.$titreSlug.'">'.T_("Voir le snippet").'</a><br />', 'success');               
    }
    
    // Suppression d'un point de restauration
    if(isset($_GET['del']))
    { 
        $restore = $_GET['del'];
        $id = $_GET['id'];
        
        $bashDir = KT_ROOT.DS.D_CORE.DS;
        $dir = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$id.DS.'save'.DS.$restore.DS;        
        
        $st = KTdeleteDirectory($dir);
        
        if($st){
            $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("Le point de restauration a été supprimé"), 'success');            
        }else
            $msg = KTMakeDiv('ALERT', 'alert alert-alert el_top40 text-center wBold', T_("Une erreur s'est produite lors de la suppression du point de restauration"), 'alert');   
        
        // Raffraîchissement de la page après 1 seconde
        header('Refresh: 1; url= restore?id='.$id); 
    }
     
    // Lien pour retourner à la page consultée
    $btnRetourPageConsultee =  '<div class=""><a class="btn btn-primary" href="page?id='.$id.'">Retourner sur la page</a></div>'; 
    

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_restore.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);

$engine->set_var('creer-snippet', '<a class="btn btn-success btn-xs" href="{url-primary-creer}">'.T_("Créer un snippet").'</a>');
$engine->set_var('list-snippet', '<a class="btn btn-primary btn-xs" href="{url-primary-index}">'.T_("Accueil").'</a>');

$engine->set_var('titre-restore', T_("Restaurer une version précédente"));
$engine->set_var('trm-page-restore', T_("Points de restauration du snippet"));
$engine->set_var('liste-version', $versionsListe);
$engine->set_var('btn-retour', $btnRetourPageConsultee);
$engine->set_var('titre-page', $datas['Titre']);
$engine->set_var('previsualisation', $previsualisation);

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';


// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
  
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );


