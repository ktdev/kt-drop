<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// ********************************* CONTROLE DE L'ACCES *********************************
// ***************************************************************************************

// Module en cours
$pathAndQuery = null;
$pathAndQuery = getPathAndQueryFromUrl(K_PTH_INST, $_SERVER ['REQUEST_URI']);
$route = new Router($pathAndQuery['path']);
$currentModule = $route->getCurrentModule();

// Vérification de l'accès au module
$users = new UsersCodes();
$access = $users->KTgetAccessModule($currentModule, $_SESSION['role']);

// DROITS AUTORISES : |NO| = 0 / |READ| = 1 / |WRITE| = 2 / |FULL| = 3 
if($access < 2)
{
    header('Refresh: 0; url= '.K_PTH_INST . D_APP . DS . D_MODS . DS . D_HOME . DS . 'noaccess'); 
    die();
}
  

// ***************************************************************************************   
    $id_a_creer = NULL;
    $createResult = NULL;

    if(!empty($_POST['titre']) && !empty($_POST['contenu'])) // Si le formulaire a été validé et qu'il n'est pas vide
    {
        $titre = htmlspecialchars($_POST['titre']); // La variable contenant le titre
        if(!empty($_POST['comments']))
        $comments = htmlspecialchars($_POST['comments']);
        $contenu = $_POST['contenu']; // Le contenu
        $tag = $_POST['tags']; // L'étiquette'
      
        if(isset($_POST['1'])) // Si on crée la page normalement
            $createResult = creerPage('normal', $titre, $comments, $contenu, $tag); // On appelle la fonction pour créer la page
        
        if(isset($_POST['2'])) // Si on veut rendre l'édition impossible
            $createResult = creerPage('protege', $titre, $comments, $contenu, $tag); // On appelle la fonction pour créer la page

        if($createResult['status']){
            $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', $createResult['msg'].' <br> {lien-visionner}', 'success');   
            $id_a_creer = $createResult['datas'];
        }else
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $createResult['msg'], 'alert');       
    }else{
        if(isset($_POST[1]))
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', T_("Veuillez remplir les champs titre et code source"), 'alert');     

        if(isset($_POST['titre']))
            $titre = $_POST['titre'];
        else
            $titre = null;  

        if(isset($_POST['comments']))
            $comments = $_POST['comments'];
        else
            $comments = null; 

        if(isset($_POST['contenu']))
            $contenu = $_POST['contenu'];
        else
            $contenu = null;
    }    
    
// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_creer.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/** 
 * Add widgetEditor CSS rules and JS script for WidgetEditor (Trumbowyg)
 */
$engine->set_var('head_section_addons', insertHeadSection(buildLink(K_PTH_THM)));
$engine->set_var('footer-js-section-addons', insertFooterJsSection(buildLink(K_PTH_THM)));



// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);

// Variables générales
$engine->set_var('titre-page', T_('Créer un nouveau Snippet'));
$engine->set_var('creer-snippet', '<a class="btn btn-success btn-xs" href="{url-primary-creer}">'.T_("Créer un snippet").'</a>');
$engine->set_var('accueil-page', '<a class="btn btn-primary btn-xs" href="{url-primary-index}">'.T_("Accueil").'</a>');
$engine->set_var('lien-visionner','<a class="wRedColor" href="{url-primary-page}?id='.$id_a_creer.'">'.T_("Voir le snippet").'</a>');
$engine->set_var('gestion-snippets', '<a class="btn btn-success btn-xs" href="{url-primary-adm-pages}"><i class="fa fa-cog "></i> '.T_("Gérer").'</a>');

$engine->set_var('valeur-titre', $titre);
$engine->set_var('valeur-contenu',$contenu);
$engine->set_var('value-comments', $comments);
$tagsList = KTLoadTagsList();
$engine->set_var('select-tags', KTSelectTags($tagsList));  

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// After defined


// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{

} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );


