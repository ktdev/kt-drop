<?php


//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++   

// ********************************* CONTROLE DE L'ACCES *********************************
// ***************************************************************************************

// Module en cours
$pathAndQuery = null;
$pathAndQuery = getPathAndQueryFromUrl(K_PTH_INST, $_SERVER ['REQUEST_URI']);
$route = new Router($pathAndQuery['path']);
$currentModule = $route->getCurrentModule();

// Vérification de l'accès au module
$users = new UsersCodes();
$access = $users->KTgetAccessModule($currentModule, $_SESSION['role']);

// DROITS AUTORISES : |NO| = 0 / |READ| = 1 / |WRITE| = 2 / |FULL| = 3 
if($access < 3)
{
   header('Refresh: 0; url= '.K_PTH_INST . D_APP . DS . D_MODS . DS . D_HOME . DS . 'noaccess'); 
   die();
}
// ***************************************************************************************

// Lister les pages
$codes = listePages();
$compteur = 0;
$listePages = NULL;

$listePages = '<div class="snippets-list">';

foreach($codes as $code) // On explore le tableau
{
    $listePages .= 
            '<div class="row-page">'.
            '<span class="pull-left"><a href="page?id='.$code['Id'].'">'.$code['Titre'].'</a></span>'.
            '<span class="pull-right row-page-right"> '.
            '<span class="tag text-center">'.strtoupper(KTGetTagById(trim($code['TagID']))).'</span>'.
            '<span class="author text-center"><i class="fa fa-user fa-lg" data-toggle="tooltip" data-placement="right" title="'.$code['Author'].'"></i></span>
             <span class="lastupdate  mRight10"><i class="fa fa-calendar fa-lg" data-toggle="tooltip" data-placement="right" title="'.$code['LastUpdate'].'"></i></span>'.
            '<a href="edit?id='.$code['Id'].'"><i class="fa fa-pencil-square-o fa-lg suite-icons" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="'.T_("Editer").'"></i></a>  '.
            '<a href="adm-pages?copy='.$code['Id'].'"><i class="fa fa-files-o fa-lg suite-icons" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="'.T_("Dupliquer").'"></i></a>  '.
            '<a href="#" class="deleteSnip" data-id="'.$code['Id'].'"><i class="fa fa-trash fa-lg suite-icons el_error" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="'.T_("Supprimer").'"></i></a>  '.
            '</span>'.
            '<div class="clearfix"></div>'. 
            '</div>';
    $compteur++;
}

$listePages .= '</div>';

// Dupliquer une page
if(isset($_GET['copy']))
{ 
    $id = $_GET['copy'];
    
    $bashDir = KT_ROOT.DS.D_CORE.DS;
    $dir = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$id.DS; 
    $dircopy = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$id.'-copy'.DS; 

    $st = KTcopyDirectory($dir, $dircopy);
    

    if($st){
        // Ue fois la copie effectuée on va rechercher le titre de la source 
        $titre = file_get_contents($dir.'titre.txt');
        // On modifie le titre en lui concatènant la chaîne '-copy'
        $changeTitre = file_put_contents($dircopy.DS.'titre.txt', $titre.'-copy');
        $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("Le snippet a été dupliqué"), 'success');            
    }else
        $msg = KTMakeDiv('ALERT', 'alert alert-alert el_top40 text-center wBold', T_("Une erreur s'est produite lors de l'opération de duplication du snippet"), 'alert');   

    // Raffraîchissement de la page après 1 seconde
    header('Refresh: 1; url= adm-pages'); 
}

// Suppression d'une page
if(isset($_GET['delid']))
{ 
    
    //DEBUG//echo'DELETE'; die();

    $id = $_GET['delid'];
    
    $bashDir = KT_ROOT.DS.D_CORE.DS;
    $dir = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$id.DS;        

    $st = KTdeleteDirectory($dir);

    if($st){
        $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("La snippet a été supprimé"), 'success');            
    }else
        $msg = KTMakeDiv('ALERT', 'alert alert-alert el_top40 text-center wBold', T_("Une erreur s'est produite lors de la suppression du snippet"), 'alert');   

    // Raffraîchissement de la page après 1 seconde
    header('Refresh: '.K_REFRESH.'; url= adm-pages'); 
}

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_ADM, 'tpl_adm_pages.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Variables et termes à afficher
$engine->set_var('titre-page', T_("Gestion des snippets"));
$engine->set_var('creer-page', '<a class="btn btn-success btn-xs" href="{url-primary-creer}">'.T_("Créer un snippet").'</a>');
$engine->set_var('accueil-page', '<a class="btn btn-primary btn-xs" href="{url-primary-index}">'.T_("Accueil").'</a>');
$engine->set_var('liste-pages', $listePages);
$engine->set_var('modale-delete', displayModalDelete('{trm-supprimer-snippet}','{trm-supprimer-snippet-confirm}')); 


// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{

} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_ADM );

// Rendu du template
$engine->p( 'display' );

