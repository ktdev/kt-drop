<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

// ********************************* CONTROLE DE L'ACCES *********************************
// ***************************************************************************************

// Module en cours
$pathAndQuery = null;
$pathAndQuery = getPathAndQueryFromUrl(K_PTH_INST, $_SERVER ['REQUEST_URI']);
$route = new Router($pathAndQuery['path']);
$currentModule = $route->getCurrentModule();

// Vérification de l'accès au module
$users = new UsersCodes();
$access = $users->KTgetAccessModule($currentModule, $_SESSION['role']);

// ***************************************************************************************
    
// Lister les pages
$codes = listePages();
/*
echo'<pre>';
var_dump($codes);   
echo'</pre>';
*/

$compteur = 0;
$listePages = NULL;

$listePages = '<div class="container-listePages">';

foreach($codes as $code) // On explore le tableau
{
    
    if($access >= 2 )
        $editAccess = '<a href="edit?id='.$code['Id'].'"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="'.T_("Editer").'"></i></a>';
    else
    $editAccess = "";

    $listePages .= 
            '<div class="row-page">'.
            '<div class="clearfix"></div>'. 
            '<span class="pull-left"><a href="page?id='.$code['Id'].'">'.$code['Titre'].'</a></span>
             <span class="pull-right">
             <span class="tag text-center">'.strtoupper(KTGetTagById(trim($code['TagID']))).'</span>
             <span class="author text-center"><i class="fa fa-user fa-lg" data-toggle="tooltip" data-placement="right" title="'.$code['Author'].'"></i></span>
             <span class="lastupdate  mRight10"><i class="fa fa-calendar fa-lg" data-toggle="tooltip" data-placement="right" title="'.$code['LastUpdate'].'"></i></span>
             '.$editAccess.'
             </span>';
    
    
    $listePages .=
            '<div class="clearfix"></div>'. 
            '</div>';
    $compteur++;
}

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_index.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt-welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);
$engine->set_var('liste-pages', $listePages);
$engine->set_var('titre-liste-page', T_("Liste des snippets"));

// Menu Secondaire
if($access >= 2 )
{   
    $engine->set_var('new-snippets', '<a class="btn-sm btn-success" href="{url-primary-creer}">'.T_("Créer un snippet").'</a>');
    $engine->set_var('gestion-snippets', '<a class="btn-sm btn-success" href="{url-primary-adm-pages}"><i class="fa fa-cog"></i> '.T_("Gérer").'</a>');
}


// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';


// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{

} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );


