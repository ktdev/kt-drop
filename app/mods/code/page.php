
<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

// ********************************* CONTROLE DE L'ACCES *********************************
// ***************************************************************************************

// Module en cours
$pathAndQuery = null;
$pathAndQuery = getPathAndQueryFromUrl(K_PTH_INST, $_SERVER ['REQUEST_URI']);
$route = new Router($pathAndQuery['path']);
$currentModule = $route->getCurrentModule();

// Vérification de l'accès au module
$users = new UsersCodes();
$access = $users->KTgetAccessModule($currentModule, $_SESSION['role']);

// ***************************************************************************************

$pageTitre = NULL;
$pageComments = NULL;
$pageContenu = NULL;

if(empty($_GET['id'])) // Si on n'a pas fourni en paramètre l'id de la page à voir
 {
    $msg = T_("Vous devez fournir l'id de la page");  
    $id = NULL;
 }else{
    $id =  $_GET['id'];
    $donnees = viewPage($id);
    $pageTitre = $donnees['Titre'];
    $pageComments = $donnees['Comments'];
    $pageContenu = $donnees['Contenu'];
    $pageAuthor = $donnees['Author'];
    $pageLastUpdate = $donnees['LastUpdate'];
    $pageTagID = strtoupper(KTGetTagById(trim($donnees['TagID'])));

    //echo 'TAG : '.$donnees['TagID']; die();
 }
// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_page.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);

// Variables générales
$engine->set_var('page-titre', $pageTitre);
$engine->set_var('page-comments', $pageComments);
$engine->set_var('page-contenu', $pageContenu);
$engine->set_var('page-author', $pageAuthor);
$engine->set_var('page-lastupdate', $pageLastUpdate);
$engine->set_var('page-tag', $pageTagID);

// Menu Secondaire
$engine->set_var('list-snippet', '<a class="btn btn-primary btn-xs" href="{url-primary-index}">'.T_("Accueil").'</a>');
if($access >= 2 )
{ 
   $engine->set_var('editer-snippet', '<a class="btn btn-success btn-xs" href="{url-primary-edit}?id='.$id.'">'.T_("Editer le snippet").'</a>');
   $engine->set_var('restaurer-snippet', '<a class="btn btn-success btn-xs" href="{url-primary-restore}?id='.$id.'">'.T_("Restaurer une version").'</a>');
   $engine->set_var('creer-snippet', '<a class="btn btn-success btn-xs" href="{url-primary-creer}">'.T_("Créer un snippet ").'</a>');
   $engine->set_var('gestion-snippets', '<a class="btn btn-success btn-xs" href="{url-primary-adm-pages}"><i class="fa fa-cog "></i> '.T_("Gérer").'</a>');
}

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';


// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
 
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );


