<?php


//-----------------------------------------------------------------------------
// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . DS . D_TPL . DS . D_MISC . DS );

// Assignation du template
$engine->set_file( D_MISC, 'tpl_error.htm' );
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le titre                          
$engine->set_var('titre', 'ERROR :: '.K_TITLE);

// Affiche le mesage
$engine->set_var('errorMsg', $e);

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_MISC );

// Rendu du template
$engine->p( 'display' );