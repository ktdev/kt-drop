<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////  
//                                         DEFAULT SET_VAR
/////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////// HEADERS FOOTERS ////////////////////////////////////////////
// Affiche la section Head du code HTML
$engine->set_var('head_section', insertHeadSection(buildLink(K_PTH_THM)));
// Afficher le footer                         
$engine->set_var('footer', insertFooter());
// Afficher la Footer JS Section
$engine->set_var('footer-js-section', insertFooterJsSection(buildLink(K_PTH_THM)));

/////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// METAS /////////////////////////////////////////////////
// Affiche le nom de l'application
$engine->set_var('appName', K_NAME);
// Affiche l'auteur de l'application
$engine->set_var('author', K_AUTHOR);
// Affiche le site de l'auteur de l'application
$engine->set_var('author_site', K_AUTHOR_SITE);
// Affiche la date de création de l'application
$engine->set_var('creation_date', K_DATE_CREATION);
// Affiche la description de l'application
$engine->set_var('description', K_DESCRIPTION);
// Affiche les mots clefs de l'application
$engine->set_var('keywords', K_KEYWORDS);
// Indique aux webcrawlers le délais de passage
$engine->set_var('revisit_after', K_REVISIT_AFTER);
// Indique aux webcrawlers le contenu indexable
$engine->set_var('robots', K_ROBOTS);
// Affiche la Date du jour
$engine->set_var('date_now', date('d/m/Y'));

/////////////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////// AUTRES //////////////////////////////////////////////
// Affiche la Date du jour
$engine->set_var('version', K_VER);
// Affiche la Date du jour
$engine->set_var('name_app', K_NAME);

//////////////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////// SESSIONS //////////////////////////////////////////////
// Affiche le nom du user  (nom, prénom)
if(!empty($_SESSION['pseudo'])){
     $engine->set_var('pseudo', $_SESSION['pseudo']);
      
}

if(!empty($_SESSION['email'])){
    $engine->set_var('email', $_SESSION['email']);  
}

if(!empty($_SESSION['id_user'])){
    $engine->set_var('userid', $_SESSION['id_user']);  
}
   
////////////////////////////////////////////////////////////////////////////////////////////////////  
//////////////////////////////////////// NAVBAR LINKS //////////////////////////////////////////////
// Include de la navigation
$engine->set_var('navbar', includeNavigation('.'.DS.D_CORE.DS));
$engine->set_var('menu-user', insertMenuUser());
$engine->set_var('menu-lang', insertMenuLang($query));

////////////////////////////////////////////////////////////////////////////////////////////////////  
/////////////////////////////////////////// TERMES /////////////////////////////////////////////////
$engine->set_var('trm-langue', T_('Langue'));
$engine->set_var('menu-lang-fr', T_('Français'));
$engine->set_var('menu-lang-nl', T_('Néerlandais'));
$engine->set_var('menu-lang-en', T_('Anglais'));
$engine->set_var('menu-aide', T_('Aide'));
$engine->set_var('menu-admin-index', T_('Tableau de bord'));
$engine->set_var('menu-admin-settings', T_('Paramètres'));
$engine->set_var('menu-quitter', T_('Quitter'));
$engine->set_var('menu-connecter', T_('Se connecter'));
$engine->set_var('menu-profile', T_('Profil'));
$engine->set_var('menu-snippets', T_('Snippets'));
$engine->set_var('menu-gestion-snippets', T_('Gérer les Snippets'));
$engine->set_var('menu-users-gestion', T_('Gestion des utilisateurs'));
$engine->set_var('trm-est', T_('est'));
$engine->set_var('trm-avec', T_('avec'));
$engine->set_var('trm-par', T_('par'));
$engine->set_var('trm-theme', T_('Thème'));
$engine->set_var('trm-enregistrer', T_('Enregistrer'));
$engine->set_var('trm-lecture-seule', T_('Lecture seule'));
$engine->set_var('trm-passwd', T_('Mot de passe'));
$engine->set_var('trm-confirmation', T_('Confirmation'));
$engine->set_var('trm-status', T_('Status'));
$engine->set_var('trm-actif', T_('Actif'));
$engine->set_var('trm-inactif', T_('Inactif'));
$engine->set_var('trm-role', T_('Rôle'));
$engine->set_var('trm-comments', T_('Commentaires'));
$engine->set_var('trm-administrator', T_('Administrateur'));
$engine->set_var('trm-simple-user', T_('Simple utilisateur'));
$engine->set_var('trm-champs-obligatoire', T_('Champs obigatoire'));
$engine->set_var('trm-auteur', T_('Auteur'));
$engine->set_var('trm-mis-a-jour', T_('Mis à jour'));

$engine->set_var('trm-titre', T_('Titre'));
$engine->set_var('trm-contenu', T_('Contenu'));
$engine->set_var('trm-codesource', T_('Code source'));
$engine->set_var('trm-tag', T_('Etiquette'));
$engine->set_var('trm-envoyer', T_('Envoyer'));
$engine->set_var('trm-creer', T_('Créer'));
$engine->set_var('trm-login', T_('Login'));
$engine->set_var('trm-email', T_('E-mail'));
$engine->set_var('trm-password', T_('Mot de passe'));
$engine->set_var('trm-password-confirm', T_('Confirmation mot de passe'));
$engine->set_var('trm-creer-editer-user', T_('Créer / Editer un utilisateur'));
$engine->set_var('trm-list-user', T_('Liste des utilisateurs'));
$engine->set_var('trm-modifier', T_('Modifier'));
$engine->set_var('trm-annuler', T_('Annuler'));
$engine->set_var('trm-supprimer', T_('Supprimer'));
$engine->set_var('trm-supprimer-user', T_('Supprimer un utilisateur'));
$engine->set_var('trm-supprimer-snippet', T_('Supprimer un snippet'));
$engine->set_var('trm-supprimer-snippet-confirm', T_('Etes-vous certain de supprimer ce snippet ?'));
$engine->set_var('trm-suppression-immediate', T_("Attention, la suppression est immédiate ! Il n\'y a pas de demande de confirmation"));
$engine->set_var('trm-text-intro', T_('Texte d\'introduction'));
$engine->set_var('trm-path-install-non-modifiable', T_('Le chemin d\'installation ne peut être modifié que manuellement dans le fichier'));

////////////////////////////////////////////////////////////////////////////////////////////////////  
//////////////////////////////////////////// LIENS /////////////////////////////////////////////////
$engine->set_var('url-default-index',buildLink(K_DEF_INDEX));

$engine->set_var('url-home-index',buildLink(D_APP.DS.D_MODS.DS.D_HOME.DS.'index'));
$engine->set_var('url-home-login',buildLink(D_APP.DS.D_MODS.DS.D_HOME.DS.'login'));
$engine->set_var('url-home-logout', K_PTH_MODS.D_HOME.DS.'logout');

$engine->set_var('url-user-profile', K_PTH_MODS.D_USER.DS.'profile');
$engine->set_var('url-user-edit-profile', K_PTH_MODS.D_USER.DS.'edit-profile');

$engine->set_var('url-primary-index',buildLink(D_APP.DS.D_MODS.DS.D_PRIMARY.DS.'index'));
$engine->set_var('url-primary-creer',buildLink(D_APP.DS.D_MODS.DS.D_PRIMARY.DS.'creer'));
$engine->set_var('url-primary-page',buildLink(D_APP.DS.D_MODS.DS.D_PRIMARY.DS.'page'));
$engine->set_var('url-primary-edit',buildLink(D_APP.DS.D_MODS.DS.D_PRIMARY.DS.'edit'));
$engine->set_var('url-primary-restore',buildLink(D_APP.DS.D_MODS.DS.D_PRIMARY.DS.'restore'));
$engine->set_var('url-primary-adm-pages',buildLink(D_APP.DS.D_MODS.DS.D_PRIMARY.DS.'adm-pages'));

$engine->set_var('url-admin-index', K_PTH_MODS.D_ADM.DS.'index');
$engine->set_var('url-admin-settings', K_PTH_MODS.D_ADM.DS.'settings');
$engine->set_var('url-admin-gestion-users', K_PTH_MODS.D_ADM.DS.'gestion-users');

$engine->set_var('url-help-index',buildLink(D_APP.DS.D_MODS.DS.D_HELP.DS.'index'));

$engine->set_var('url-testing-teststore',buildLink(D_APP.DS.D_MODS.DS.D_TESTING.DS.'teststore'));

//////////////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////// LOGOS /////////////////////////////////////////////////
// Affiche le logo Large                         
$engine->set_var('logo-large', buildLink(D_THEMES . DS . D_THM_USE . DS . D_IMG . DS . 'logos'. DS . 'logo-large.png'));

// Affiche le logo dans la NAVBAR                         
$engine->set_var('logo', buildLink(D_THEMES . DS . D_THM_USE . DS . D_IMG . DS . 'logos'. DS . 'logo.png')); 

////////////////////////////////////////////////////////////////////////////////////////////////////   
///////////////////////////////////////// DEBUG BAR ////////////////////////////////////////////////  
//       Affiche la "Debug Footer Bar"  -  Celle ci indique que l'application est en mode "DEBUG"
//                           et affiche le temps de génération de la page 
///////////////////////////////////////////////////////////////////////////////////////////////////*/                           
if(K_DEBUG)
{
    $dbug = new debug();
    if(isset($_SESSION['startTime'])) $engine->set_var('debug_generate', $dbug->KTGenerate($_SESSION['startTime']));
}
///////////////////////////////////////////////////////////////////////////////////////////////////*/                           