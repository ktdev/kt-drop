��    =        S   �      8     9     X     ^     c     k     s     �     �  !   �     �     �     �       '     	   5     ?     K     Z     b      j     �  &   �  .   �     �               /     8     N  "   [     ~     �     �     �     �  (   �     �     �               )     5     >     E     U  #   \  I   �  X   �     #	  H   6	     	  #   �	     �	     �	  %   �	      
  #   (
     L
     Q
     U
    Y
     t     �     �     �     �     �     �     �     �               "     )     1     K     Q     Z     h     q  #   z     �  #   �  )   �  #   �          "     ?     D  
   V     a     �     �  
   �  
   �     �     �  
   �     �     �       	     	        !  	   (     2     8  7   T  ?   �     �  9   �               8  	   M     W     u     �     �     �     �     "                     1      -   3             ,   7                         	      '      0   &   +                    4   2         :           
      ;   )              *   5   <   8               9   =       /                     6             $                    !   (   #           %      .    Accéder au module par défaut Actif Aide Anglais Bonjour Changer votre adresse e-mail Changer votre mot de passe Confirmation Contenu en cours de rédaction... Créer un snippet  Déconnexion E-mail Enregistrer Erreur lors du cryptage du mot de passe Français Identifiant Identification Inactif Inconnu L'adresse e-mail est obligatoire Langue Le champs confirmation est obligatoire Le champs nouveau mot de passe est obligatoire Le login est obligatoire Lecture seule Les champs ne correspondent pas Modifier Modifier votre profil Mot de passe Mot de passe modifié avec succès Nouveau mot de passe Nouvel e-mail Néerlandais Paramètres Profil Quelle action souhaitez-vous réaliser ? Quitter Retour sur la page d'accueil SELECTIONNER Se connecter Se souvenir Sommaire Status Tableau de bord Thème Votre adresse e-mail actuelle est:  Votre adresse e-mail ne peut-être mise à jour que par un administrateur Votre compte utilisateur est inactif, veuillez prendre contact avec votre administrateur Votre indentifiant Votre login et/ou votre mot de passe sont erronés - Veuillez réessayer Votre mot de passe Votre mot de passe a été modifié Votre profil utilisateur Vous déconnecter Vous êtes actuellement connecté sur Vous êtes à présent connecté Vous êtes à présent déconnecté avec est par Project-Id-Version: WAL
POT-Creation-Date: 2019-07-12 10:55+0200
PO-Revision-Date: 2019-07-12 10:57+0200
Last-Translator: Alain Kelleter <alain.kelleter@just.fgov.be>
Language-Team: KTDev <ken@kentaro.be>
Language: nl_BE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _;T_
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: themes/core/js
 Toegang tot de standaard module Active Helpen Engels Hallo Wijzig uw e-mailadres Wijzig uw wachtwoord Bevestiging Inhoud in voorbereiding... Maak een nieuwe object  Afmelden E-mail Opslaan Fout wachtwoord-encryptie Frans Inloggen Identificatie Inactive Onbekend De bevestiging is verplichte velden Taal De bevestiging is verplichte velden Het nieuwe wachtwoord velden is verplicht De bevestiging is verplichte velden Alleen-zelen De velden komen niet overeen Edit Bewerk je profiel Wachtwoord Wachtwoord met succes gewijzigd Nieuw wachtwoord Nieuwe e-mail Nederlands Parameters Profiel Welke actie wilt u bereiken ? Vertrekken Terug naar de home page SELECT Log in Onthouden Overzicht Status Dashboard Thema Uw huidige e-mailadres is : Uw e-mailadres kan worden bijgewerkt door een beheerder Uw account inactief is, kunt u contact opnemen met uw beheerder Uw login Uw login en / of wachtwoord onjuist - Probeer het opnieuw Uw wachtwoord Uw wachtwoord is gewijzigd Uw gebruikersprofiel Uitloggen U bent momenteel aangemeld op U bent nu verbonden U bent nu offline met is bij 