<?php

/**
 * Liste les pages du Wiki
 * @return datas
 */
function listePages() {
    
    $donnees = NULL;
    $repertoirePages = D_PRIVATE.DS.D_DATAS.DS.D_PAGES;
            
    $dossiers = glob($repertoirePages.DS.'*');
    
    // Classement des dossiers afin qu'ils soient affichés par ordre alphabétique / croissant
    asort($dossiers); 
        
    $noms = array();
    $ids = array();
    $compteur = 0;
    
    // Lister les dossier
    foreach($dossiers as $repertoire) 
    {
        // Vérifier si c'est un dossier que l'on traite et non un fichier
        if(is_dir($repertoire)) 
        {
            $id = explode(DS, $repertoire);
            
            // Nom du dossier = id de la page
            $ids[$compteur] = $id[3]; 

            // Lecture du titre
            if(file_exists($repertoire.DS.'titre.txt'))
                $titre = file_get_contents($repertoire.DS.'titre.txt');

            // Lectures des informations
            if(file_exists($repertoire.DS.'infos.ini'))
            {
                $loadAuthor = getItemIniFile($repertoire.DS.'infos.ini', 'author', 'informations');  
                $loadLastUpdate = getItemIniFile($repertoire.DS.'infos.ini', 'lastupdate', 'informations'); 
                $loadTagID = getItemIniFile($repertoire.DS.'infos.ini', 'tag', 'informations');   
            }
            
            // On place le titre dans le tableau qui contient les titres des pages 
            $code[$compteur]['Id'] = $id[3];
            $code[$compteur]['Titre'] = $titre; 
            $code[$compteur]['Author'] = $loadAuthor; 
            $code[$compteur]['LastUpdate'] = $loadLastUpdate; 
            $code[$compteur]['TagID'] = $loadTagID; 

            $compteur++; 
        }
    }

    return $code;
    
}//end of fct listePages

function countNbPages(){
    
    $repertoire = D_PRIVATE.DS.D_DATAS.DS.D_PAGES;
    $dossiers = glob($repertoire.DS.'*');
    $compteur = 0;
    foreach($dossiers as $adresse) 
    {
        // Vérifier si c'est un dossier que l'on traite et non un fichier
        if(is_dir($adresse)) 
        {
            $compteur++;
        }
    }
    
    return $compteur;
    
}

/**
 * Création d'une page
 * 
 * @param type $type
 * @param type $titre
 * @param type $contenu
 * @return boolean
 */
function creerPage($type, $titre, $comments, $contenu, $tag)
{
    $id_a_creer = NULL;
    $rTitre = NULL;
    $rTitreSave = NULL;
    $rContenu = NULL;
    $rContenuSave = NULL;
    $rComments = NULL;
    $rCommentsSave = NULL;
    
    $st = array();
    $st['status'] = true;
    $st['msg'] = NULL;
    $st['datas'] = NULL;
    
    $repertoire = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES;
    
    // "Slugification" du titre pour la creation du répertoire
    $newRepertoire = KTslugify($titre);
    
    if(!file_exists($repertoire.DS.$newRepertoire))
    {        
        
        $creer_dossier = mkdir($repertoire.DS.$newRepertoire); // On crée le dossier
        $repertoire = $repertoire.DS.$newRepertoire;

        if($type == "protege")
        {
            $creer_fichier_protect = fopen($repertoire.DS.'protect', 'w+');
            fclose($creer_fichier_protect);
        }

         // On écrit le titre dans le fichier
         $rTitre = file_put_contents($repertoire.DS.'titre.txt', $titre); 
         // On écrit le commentaire dans le fichier
             $rComments = file_put_contents($repertoire.DS.'comments.txt', $comments); 
         // On inscrit le contenu dans le fichier
             $rContenu = file_put_contents($repertoire.DS.'contenu.txt', $contenu); 
         // On inscrit le nom de l'auteur et la date de création
             $params["informations"] = array(
                 "author" => $_SESSION['pseudo'],
                 "lastupdate" => date('d/m/y H:i:s'), 
                 "tag" => $tag 
             );
 
             $createInfos = new ini ($repertoire.DS.'infos.ini');              
             $createInfos->ajouter_array($params);
             $res = $createInfos->ecrire(TRUE);
 
             
         /**
          * Partie pour les sauvegardes, pour la restauration
         **/
         // Récupèration du timestamp actuel pour dater la sauvegarde
         $timestamp_actuel = time(); 
         // Création du dossier qui va contenir toutes les sauvegardes de la page
         $creer_dossier_sav = mkdir($repertoire.DS.'save'); 
         // Création du dossier qui va contenir le titre et le contenu pour la restauration
         $creer_dossier_time = mkdir($repertoire.DS.'save'.DS.$timestamp_actuel); 
         // Mise à jour du répertoire de traitement
         $repertoire = $repertoire.DS.'save'.DS.$timestamp_actuel; 
 
         //Même étape que précédemment
         $rTitreSave = file_put_contents($repertoire.DS.'titre.txt', $titre);
         $rCommentsSave = file_put_contents($repertoire.DS.'comments.txt', $comments);
         $rContenuSave = file_put_contents($repertoire.DS.'contenu.txt', $contenu);
 
         $createParams = new ini ($repertoire.DS.'infos.ini');              
         $createParams->ajouter_array($params);
         $res = $createParams->ecrire(TRUE);

        /**
         * Fin de la partie pour les restaurations
        **/
    }else{
        $st['status'] = false;
        $st['msg'] = T_("Le répertoire existe déjà, veuillez modifier votre titre");
        $st['datas'] = NULL;
        return $st;
    }
    
    // Retours de la fonction
    if($rTitre && $rContenu && $rTitreSave &&$rContenuSave) {
        $st['status'] = true;
        $st['msg'] = T_("Votre nouvelle page a bien été créée");
        $st['datas'] = $newRepertoire;
        return $st;
    }else{
        $st['status'] = false;
        $st['msg'] = T_("L'application a rencontré un problème lors de la création de la page");
        $st['datas'] = NULL;
        return $st;
    }    
}//end of fct creerPage

/**
 * 
 * @param type $id
 * @return type
 */
function viewPage($id) {
    $donnees = null;
    $comments = null;
    $titre = null;
    $contenu = null;
    $loadAuthor = null;
    $loadLastUpdate = null;

    $repertoire = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$id;

    if(file_exists($repertoire.DS.'titre.txt'))
        $titre = file_get_contents($repertoire.DS.'titre.txt'); // On lit le fichier contenant le titre
    if(file_exists($repertoire.DS.'comments.txt'))
        $comments = file_get_contents($repertoire.DS.'comments.txt'); // On lit le fichier contenant le commentaire
    if(file_exists($repertoire.DS.'contenu.txt'))
        $contenu = file_get_contents($repertoire.DS.'contenu.txt'); // On lit le fichier contenant le contenu 
    if(file_exists($repertoire.DS.'infos.ini')){
        $loadAuthor = getItemIniFile($repertoire.DS.'infos.ini', 'author', 'informations');  
        $loadLastUpdate = getItemIniFile($repertoire.DS.'infos.ini', 'lastupdate', 'informations');  
        $tagID = getItemIniFile($repertoire.DS.'infos.ini', 'tag', 'informations');  
    }
        
    $donnees = array('Titre' => $titre, 'Comments' => $comments, 'Contenu' => $contenu, 'Author' => $loadAuthor, 'LastUpdate' => $loadLastUpdate, 'TagID' => $tagID); // On met les valeurs dans un tableau
    return $donnees;

}//end of fct viewPage

function editPage($id, $titre, $comments, $contenu, $titreSlug = NULL, $author, $lastupdate, $tag)
{
    $repertoireOld = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$id;
    $repertoireNew = KT_ROOT.DS.D_PRIVATE.DS.D_DATAS.DS.D_PAGES.DS.$titreSlug;
    $rRenommage = NULL;

    // Initialisation des commentaires
    if(empty($comments)){
        //DEBUG//echo ' PAS COMMENTS';
        $comments = " ";
    }
        
    
    // Si le titre a changé - Renommage du répertoire
    //DEBUG//echo 'ID '. $id.' - slug '.$titreSlug;
    if ($id != $titreSlug)
    {
        
        // Renommage du répertoire
        $st = KTrenameDirectory($repertoireOld, $repertoireNew);
        
        // Vérifier si le nom du répertoire a changé 
        if(is_dir($repertoireNew)){
            $rRenommage = true;
            $repertoire = $repertoireNew;
        }else{
            $rRenommage = false;   
            $repertoire = $repertoireOld;
        }
        //DEBUG//echo' diff';
        
    }else{
        $repertoire = $repertoireOld;
        $rRenommage = true;
        //DEBUG//echo' pasdiff';
    }
    
    // Ecrire le titre dans le fichier titre.txt
    $rTitre = file_put_contents($repertoire.DS.'titre.txt', $titre); 
    // Ecrire le commentaire dans le fichier commentaire.txt
    $rComments = file_put_contents($repertoire.DS.'comments.txt', $comments); 
    // Ecrire le contenu dans le fichier contenu.txt
    $rContenu = file_put_contents($repertoire.DS.'contenu.txt', $contenu); 
    // On inscrit le nom de l'auteur et la date de création
    $params["informations"] = array(
        "author" => $author,
        "lastupdate" => date('d/m/y H:i:s'), 
        "tag" => $tag 
    );

    $createInfos = new ini ($repertoire.DS.'infos.ini');              
    $createInfos->ajouter_array($params);
    $res = $createInfos->ecrire(TRUE);
   
    
    
    /**
      * PARTIE POUR LES SAUVEGARDES
    **/
    $timestamp_actuel = time(); // Timestamp actuel
    $creer_dossier_time = mkdir($repertoire.DS.'save'.DS.$timestamp_actuel); // On crée le répertoire qui aura pour nom le timestamp actuel
    
    $rTitreSave = file_put_contents($repertoire.DS.'save'.DS.$timestamp_actuel.DS.'titre.txt', $titre); // On crée le fichier et on écrit le titre
    $rCommentsSave = file_put_contents($repertoire.DS.'save'.DS.$timestamp_actuel.DS.'comments.txt', $comments); // On crée le fichier et on écrit le commentaire
    $rContenuSave = file_put_contents($repertoire.DS.'save'.DS.$timestamp_actuel.DS.'contenu.txt', $contenu); // Création du fichier + écriture du contenu

    $createParams = new ini ($repertoire.DS.'save'.DS.$timestamp_actuel.DS.'infos.ini');              
    $createParams->ajouter_array($params);
    $res = $createParams->ecrire(TRUE);

    /**
     * FIN PARTIE POUR LES SAUVEGARDES    
    **/
    
    // Retour de la fonction
    //DEBUG// var_dump($rTitre);var_dump($rComments);var_dump($rContenu);var_dump($rTitreSave);var_dump($rContenuSave);var_dump($rRenommage);

    if($rTitre != false && $rComments != false && $rContenu != false && $rTitreSave != false && $rContenuSave != false && $rRenommage != false)
    {
        //DEBUG//echo ' OK';
        return true;
    }else{
        //DEBUG//echo ' PASOK';
        return false;
    }
       
    
    
}//end of fct editPage

/**
*  Remplace caractères accentués d'une chaine 
* 
* @param string $str
* @return string
*/
function KTremoveAccents($str)
{
    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð',
        'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã',
        'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ',
        'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ',
        'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę',
        'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī',
        'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ',
        'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ',
        'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 
        'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 
        'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ',
        'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');

    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O',
        'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c',
        'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u',
        'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D',
        'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g',
        'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K',
        'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o',
        'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S',
        's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W',
        'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i',
        'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
    return str_replace($a, $b, $str);
}

/**
* Retroure une chaîne de caractère sous la forme d'un slug
* 
* @param string $text
* @return string
*/
function KTslugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // remove accents
    $text = KTremoveAccents($text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text))
    {
        return 'na-'.getToken();
    }

    return $text;
}

/**
 * Supprimer un répertoire et son contenu
 */
function KTdeleteDirectory($directory){

    $status = false;
            
    try{

        $status = rmAllDir($directory);
        return $status;
        
    }catch(Exception $e){
        return $e;
    } 
}

/**
 * Renommer un répertoire 
 */
function KTrenameDirectory($oldName, $newName){

    $status = false;
            
    try{

        $status = renameDir($oldName, $newName);
        return $status;
        
    }catch(Exception $e){
        return $e;
    } 
}

/**
 * Copier/Dupliquer un répertoire 
 */
function KTcopyDirectory($dir, $dirCopy){
        
    try{

        $st = copyDir($dir, $dirCopy);
        if($st) return true; else return false;        
        
    }catch(Exception $e){
        return $e;
    } 
}

/**
 * Retourne le contenu d'un fichier texte
 */
function KTloadContentFile($dir, $filename){

    try
    {
        $content = file_get_contents($dir.$filename); 
        return $content;

    }catch(Exceptions $e){
        return false;
    }
   
}

/**
 * Charge le talbeau de tags
 */
function KTLoadTagsList(){

    require KT_ROOT.DS.D_PRIVATE.DS.D_PARAMS.DS.'tags.php';
    return $tags;
}

/**
 * Génère une select list des Tags
 */
function KTSelectTags($tagsList, $selected = null)
{
    $str = null;
    $str .= ' <select class="form-control" id="tags" name="tags">';
    foreach($tagsList as $id => $name)
    {
        if($selected == $id)
            $str .= '<option value="'.$id .'" selected>'.$name.'</option>';
        else
            $str .= '<option value="'.$id .'">'.$name.'</option>';
    }
    $str .= ' </select>';
    return $str;
}

/**
 * Retourne le nom de l'étiquette en fonction de son ID
 */
function KTGetTagById($id){
    $tagsList = KTLoadTagsList();
    return $tagsList[$id];
} 

/**
* Génère et retourne un token
* 
*/
function getToken()
{
    return sha1(uniqid(rand()));
}
