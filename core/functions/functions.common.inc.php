<?php


//-----------------------------------------------------------------------------

/**
 * Retourne le hash du mot de passe passé en paramètre ou false
 *
 * @param string $passwd
 * @return false or hash password
 */
function KTHashPasswd($passwd) {
    try{
        // Récupérer la valeur de cost de l'application
        $dirParams = KT_ROOT.DS.D_PRIVATE.DS.D_PARAMS.DS.DS;
        $costAppValue = KTloadContentFile($dirParams, 'cost.php');

        $options = [
            'cost' => $costAppValue       
        ];

        $hashPasswd = password_hash($passwd, PASSWORD_BCRYPT, $options);
        return $hashPasswd;

    }catch(Exceptions $e){
        return false;
    }    
}

/**
* Function Installer
*
* @param string $mode : 'CTRL_INSTALL'
*/
function KTInstaller($mode) {
    // Vérifie l'existance du fichier INSTALL.txt
    switch ($mode) {
        case 'CTRL_INSTALL' :

            $installFile = 'INSTALL.txt';
            if (file_exists(ABSPATH . $installFile)) {
                $_SESSION['installFile'] = true;
                // DEBUG // echo '<p>INSTALL file exist :' . ABSPATH . $installFile.'</p>';
                return true;
            } else {
                $_SESSION['installFile'] = false;
                // DEBUG // echo '<p>INSTALL file do not exist ! :' . ABSPATH . $installFile.'</p>';
                return false;
            }
    }
}

/**
* Retourne la valeur du paramètre recherché
* @param unknown $array
* @param unknown $key
* @return unknown
*/
function KTFindParam($array, $key) {
    foreach ($array as $param) {
        if (in_array($key, $param)) {
            return $param['value'];
        }
    }

    return FALSE;
}

/**
* Retourne la valeur de la clé recherchée
*
* @param array $array
* @param string $search
*/
function KTFindKey($array, $search) {
    foreach ($array as $key => $val) {
        if ($key == $search) {
            return $val;
        }
    }

    return FALSE;
}

/**
* Petite fonction qui converti une date au format FR vers EN
*
* @param date $mydate
*/
function convertDateFrToEn($mydate) {
    $array_date_fr = explode('/', $mydate);
    $date_us = $array_date_fr[2] . "-" . $array_date_fr[1] . "-" . $array_date_fr[0];
    return $date_us;
}

/**
* Petite fonction qui converti une date au format EN vers FR
*
* @param date $mydate
*/
function convertDateEnToFr($mydate) {
    @list($annee, $mois, $jour) = explode('-', $mydate);
    return @date('d/m/Y', mktime(0, 0, 0, $mois, $jour, $annee));
}

/**
* Fonction qui produit des liens en fonction de certains paramètres
*
* @param string $link
*/
function buildLink($link) {

    // Initialisation
    $linkBuild = NULL;
    $modulePrePath = '..' . DS . '..' . DS . '..' . DS;

    // Récupére le chemin d'installation
    $dir = KT_ROOT.DS.D_PRIVATE.DS.D_PARAMS.DS;
    $pathInstall = KTloadContentFile($dir, 'path-install.php');

    // Supprime le double slash '//'
    //$specialPath = str_replace ('//', '/', $specialPath);
    // Si l'application est installée à la racine de l'hébergement
    if ($pathInstall == '/') {

        // Si on accède à la racine de l'application, cette dernière redirige vers le module et la page par défaut
        if ($GLOBALS['URI_ROOT'] === true) {
            $linkBuild = $link;
            // Sinon on accède directement à un module et une page (via URL)
        } else {
            $linkBuild = $modulePrePath . $link;
        }
        // Sinon l'application est installée dans un sous répertoire
    } else {

        // Si on accède à la racine de l'application, cette dernière redirige vers le module et la page par défaut
        if ($GLOBALS['URI_ROOT'] === true) {
            $linkBuild = $link;
            // Sinon on accède directement à un module et une page (via URL)
        } else {
            $linkBuild = $modulePrePath . $link;
        }
    }
    return $linkBuild;
}

/**
* test si le store utilisé est autorisé
*
*  @return bool
*
*/
function storesecurity() {
    if(!empty($_SESSION['IDENTIFY']))
        return true;
    else{
        return false;
    }
}
/**
* Retourne la chaîne passée en paramètre en majuscule
* Gère correctement les caractères accentués
* 
* @param string $string
*/
function strtoupperFr($string) {

    $string = strtoupper($string);
    $string = str_replace(
        array('é', 'è', 'ê', 'ë', 'à', 'â', 'î', 'ï', 'ô', 'ù', 'û'),
        array('É', 'È', 'Ê', 'Ë', 'À', 'Â', 'Î', 'Ï', 'Ô', 'Ù', 'Û'), 
        $string 
    ); 
    return $string;

}

/**
* Fonction qui retourne les sous-répertoires d'un répertoire passer en paramètre ou false
* 
* @param string $folder
*/
function KTgetSubFolder($folder) {

    $dir = @opendir($folder);

    if($dir != false)
    {
        while ($file = readDir($dir)) {
            if (($file!=".")&&($file!="..")) {
                if (is_dir("$folder/$file")) 
                    $subFolder[] = $file;    
            }
        }
        // Close directory
        closeDir($dir);
        return $subFolder;
    }else {
        return false;    
    }

}

/**
* Génération aléatoire d'un nombre
* Par défaut sur 4 chiffres
* 
* @param integer $e
*/
function KTrands($e=4) {

    // Generation number
    $nrand = '';
    for($i=0;$i<$e;$i++)
    {
        $nrand .= mt_rand(1, 9);
    }

    // Return number.
    return $nrand;
}

/***
 * Supprime un répertoire et son contenu
*/
function rmAllDir($strDirectory){

    try{

        $dir_iterator = new RecursiveDirectoryIterator($strDirectory);
        $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::CHILD_FIRST);

        // On supprime chaque dossier et chaque fichier	du dossier cible
        foreach($iterator as $fichier){

            if(substr($fichier, -1, 1) != '.')
            {
                $fichier->isDir() ? @rmdir($fichier) : @unlink($fichier);
            }
        }

        // On supprime le dossier cible
        $retrn = @rmdir($strDirectory);
        if($retrn)
            return true;
        else
             return false;

    }catch(Exception $e){
        return false;
    }

}

/**
 * Copie un répertoire source et son contenu vers un répertoire de destination
 */
function copyDir($dir, $dircopy)
{
    
    try {

        $dir_source = $dir;
        $dir_dest = $dircopy;

        mkdir($dir_dest, 0775);

        $dir_iterator = new RecursiveDirectoryIterator($dir_source, RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

        foreach($iterator as $element)
        {

            if($element->isDir()){
                @mkdir($dir_dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            } else{
                copy($element, $dir_dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            }
        }

        return true;

    } catch (Exceptions $e) {
        return $e;
    }
}

/**
 * Renommer un répertoire
 */
function renameDir($oldName, $newName)
{
   
    try {

        // Copie du répertoire cible vers le répertoire source
        $stCopy = copyDir($oldName, $newName);

        // On efface l'ancien répertoire
        $stRemove = rmAllDir($oldName);

        if($stCopy && $stRemove)
            return true;
        else
            return false;
        

    } catch (Exceptions $e) {
        return $e;
    }
    
}

/**
 * Modifie les droits d'un répertoire donné et ses sous-répertoires.
 */
function fixPermissions($path, $filemode = 0775, $dirmode = 0775)
{
    $dir = new DirectoryIterator($path);
    foreach ($dir as $item) {
        if (!$item->isDot() && !$item->isLink()) {
            if ($item->isFile()) {
                if (chmod($item->getPathname(), $filemode)) {
                    echo "CHMOD file ".$item->getPathname()." to ".decoct($filemode)." successful\r\n<br>";
                } else {
                    echo "CHMOD file ".$item->getPathname()." to ".decoct($filemode)." FAILED\r\n<br>";
                }
            }
            if ($item->isDir()) {
                if (chmod($item->getPathname(), $dirmode)) {
                    echo "CHMOD directory ".$item->getPathname()." to ".decoct($dirmode)." successful\r\n<br>";
                } else {
                    echo "CHMOD directory ".$item->getPathname()." to ".decoct($dirmode)." FAILED\r\n<br>";
                }
                fixPermissions($item->getPathname());
            }
        }
    }
}

function getItemIniFile($file, $item_recherche, $groupe_recherche)
{
    $valeur = false;

    if(file_exists($file) && $fichier_lecture=file($file))
    {
        foreach($fichier_lecture as $ligne)
        {
            $ligne_propre = trim($ligne);

            if(preg_match("#^\[(.+)\]$#",$ligne_propre,$matches)){
                $groupe = $matches[1];
            }else{

                if($groupe == $groupe_recherche)
                {
                    if(strpos($ligne,$item_recherche."=")=== 0)
                    {
                        $expl = @explode("=",$ligne,2);    
                        $valeur = end($expl);
                    }elseif($ligne == $item_recherche)
                        $valeur = '';
                }

            }
        }
    }else{
        return 'PARAMETER ERROR';
    }

    if($valeur === FALSE)
        // Groupe ou item inexistant
        return FALSE;
    else
        return $valeur;

}

/**
 * Test la validité d'un mot de passe selon les critères ci-dessous
 * 
 * preg_match() permet de rechercher une ou plusieurs occurrences d'un caractère. 
 * On l'utilise ici pour tester la présence de nos différentes familles dans la chaîne$password.
 * Le symbole# délimite le regex.
 * Le symbole^ placé au début indique tout simplement le début de la chaine.
 * (?=.*[a-z]) permet de tester la présence de minuscules.
 * (?=.*[A-Z]) permet de tester la présence de majuscules.
 * (?=.*[0-9]) permet de tester la présence de chiffres.
 * (?=.*\W) permet de tester la présence de caractères spéciaux (\W indique ce qui ne correspond pas à un mot).
 * {x,} x = longeur du mot de passe  (ex.: 8)
 *                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
 */
function KTvalidPassword($passwd, int $lenght)
{
    if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{'.$lenght.',}$#', $passwd))
        return true;
    else    
        return false;
}

function getPathAndQueryFromUrl($pathInstall, $request_uri)
{
    $pathAndQuery = array();
    
    // Si le cheminb d'installation est à la racine de l'hébergement
    if($pathInstall == '/')
    {
        // Supprime le premier slash '/' de la requête
        if($request_uri != "/")
            $request = substr($request_uri, 1); 

    // Sinon on supprimer le chemin d'installation    
    }else{
        $request = str_replace($pathInstall, "", $request_uri);
    }

    // Parsage de l'url
    $urlParsed = parse_url($request);

    // Récupérer les clés path et query (si existantes) 
    $pathAndQuery['path'] = KTFindKey( $urlParsed, 'path' );
    $pathAndQuery['query'] = KTFindKey( $urlParsed, 'query' );  
    
    // Retourne le tableau $pathAndQuerty
    return $pathAndQuery;
}
