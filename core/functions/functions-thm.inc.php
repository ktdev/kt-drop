<?php

//-----------------------------------------------------------------------------
/**
* Ajoute au code HTML:
*  
* Certaines balises <meta>
* La balise <title>
* Le chargement des feuilles de styles <link>
* 
* @param string $pathTHEME
*/
function insertHeadSection($pathTHEME, $addOns = NULL)
{


    $html = '
    <meta charset="UTF-8">
    <title>'.K_NAME.' - '.K_SURNAME.'</title>   
    <meta name="author" content="{author} | {author_site}">
    <meta name="creation-date" content="{creation_date}">
    <meta name="description" content="{description}">
    <meta name="keywords" content="{keywords}">
    <meta name="revisit-after" content="{revisit_after}">
    <meta name="robots" content="{robots}"> 
    <!-- Responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicons -->
    <link rel="shortcut icon" type="image/x-icon" href="'.$pathTHEME.D_THM_USE.DS.D_IMG.DS.'favicons'.DS.'favicon.ico" />
    <link rel="icon" type="image/x-icon" href="'.$pathTHEME.D_THM_USE.DS.D_IMG.DS.'favicons'.DS.'favicon.ico" />
    <!-- CSS -->
    <link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.D_BOOTS.DS.K_BOOTS_VER.DS.D_CSS.DS.'bootstrap.min.css">
    <link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.'font-awesome'.DS.D_CSS.DS.'font-awesome.min.css">
    <!--<link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.D_JS.DS.'highlight'.DS.'styles'.DS.'darkula.css">-->
    <link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'lib'.DS.'codemirror.css">
    <link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'addon'.DS.'display'.DS.'fullscreen.css">
    <link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'theme'.DS.'material.css">
    <link rel="stylesheet" href="'.$pathTHEME.D_THM_USE.DS.D_CSS.DS.'styles-base.css">
    <link rel="stylesheet" href="'.$pathTHEME.D_THM_USE.DS.D_CSS.DS.D_THM_USE.'.css">        
    
    ';
    $html .= $addOns;
    return $html;
}

function insertCSSDatatables($pathTHEME){
    $html = '<link rel="stylesheet" href="'.$pathTHEME.D_THM_USE.DS.D_CSS.DS.'datatables.css">';
    return $html;
}

/**
* Insert du code html pout afficher le footer de l'app
* 
*/
function insertFooter(){

    $html ='
    <div class="foot text-center"><a href="'.K_APP_SRC.'">'.K_NAME.' '.K_VER.'</a> {trm-est} <i class="fa fa-code"></i> {trm-par} <a href="'.K_AUTHOR_SITE.'">'.K_AUTHOR.'</a> - '.K_YEAR.'</div>
    ';  

    return $html;
}



/**
* Insert dans le code HTML la structure HTML du système
* de navigation principal de l'application
* 
*/
function includeNavigation($path)
{
    $incl = file_get_contents(ABSPATH.$path.'navigation.inc.php');
    return $incl;
}


/**
* Ajoute dans la structure HTML de la navigation
* le code HTML du menu user
* 
* @param string $path
*/
function insertMenuUser()
{

     // Vérification de l'accès au module
    $users = new UsersCodes();
    $access = $users->KTgetAccessModule('_menu', @$_SESSION['role']);
    
    $html = '';

    if(isset($_SESSION['IDENTIFY']) && $_SESSION['IDENTIFY'] == 0 )
        $html = '<li><a href="{url-home-login}">{menu-connecter}</a></li>';
    else{
        if(!isset($_SESSION['pseudo'])) $_SESSION['pseudo']  = T_('Inconnu');
        
        $html .= '
        <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="wYellowColor">'.$_SESSION['pseudo'].'</span> <span class="caret"></span></a>
        <ul class="dropdown-menu">
        <li><a href="{url-primary-index}">{menu-snippets}</a></li>';
        
        // Si FULL ACCES
        if($access == 3)
            $html .= '
            <li><a href="{url-primary-adm-pages}">{menu-gestion-snippets}</a></li>
            <li><a href="{url-admin-index}">{menu-admin-index}</a></li>
            <li><a href="{url-admin-gestion-users}">{menu-users-gestion}</a></li>
            <li><a href="{url-admin-settings}">{menu-admin-settings}</a></li>';       
            
        $html .= '
        <li><a href="{url-user-profile}">{menu-profile}</a></li>
        <li role="separator" class="divider usermenu"></li>
        <li><a href="{url-home-logout}"><span class="wRedColor menu-quitter">{menu-quitter}</span></a></li>
        </ul>
        </li>
        '; 
    }

    return $html;
}

/**
* Ajoute dans la structure HTML de la navigation
* le menu des langues
* 
* @param string $query : Paramètres de la requète !!! La variable $query est initialisée dans le bootstrap
* 
*/
function insertMenuLang($query) {
    
    $checkedColorFr = '';
    $checkedColorEn = '';
    $checkedColorNl = '';
    $txtColorFr = '';
    $txtColorEn = '';
    $txtColorNl = '';
    
    // Supprimer les locales si déjà existantes dans l'url en vérifiant les 
    // sous-répertoires du répertoire locale
    $subFolders = KTgetSubFolder(ABSPATH.DS.D_CORE.DS.'locale');
    if($subFolders != false)
    foreach($subFolders as $key=>$value) {
         $query = str_replace('&locale='.$value, '', $query);        
    }
    

    if(!isset($_SESSION['locale'])) $_SESSION['locale']  = DEF_LOCALE;

    switch($_SESSION['locale']) {

        case 'fr_BE': 
            $checkedColorFr = '<i class="fa fa-check wLGreenColor"></i>';
            $checkedColorEn = '<i class=""></i>';
            $checkedColorNl = '<i class=""></i>';
            $txtColorFr  = 'wGreenColor';
            $txtColorEn  = '';
            $txtColorNl  = '';
            break;
        case 'nl_BE': 
            $checkedColorFr = '<i class=""></i>';
            $checkedColorEn = '<i class=""></i>';
            $checkedColorNl = '<i class="fa fa-check wLGreenColor"></i>';
            $txtColorFr  = '';
            $txtColorEn  = '';
            $txtColorNl  = 'wGreenColor';
            break;
        case 'en_US': 
            $checkedColorFr = '<i class=""></i>';
            $checkedColorEn = '<i class="fa fa-check wLGreenColor"></i>';
            $checkedColorNl = '<i class=""></i>';
            $txtColorFr  = '';
            $txtColorEn  = 'wGreenColor';
            $txtColorNl  = '';
            break;
        default :
            $checkedColorFr = '<i class="fa fa-check wLGreenColor"></i>';
            $checkedColorEn = '<i class=""></i>';
            $checkedColorNl = '<i class=""></i>';
            $txtColorFr  = 'wGreenColor';
            $txtColorEn  = '';
            $txtColorNl  = '';

    }

    $html = '
    <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{trm-langue} <span class="caret"></span></a>
    <ul class="dropdown-menu">
    <li><a href="?'.$query.'&locale=fr_BE">'.$checkedColorFr.' <span class="'.$txtColorFr.'">{menu-lang-fr}</span></a></li>
    <li><a href="?'.$query.'&locale=nl_BE">'.$checkedColorNl.' <span class="'.$txtColorNl.'">{menu-lang-nl}</span></a></li>
    <li><a href="?'.$query.'&locale=en_US">'.$checkedColorEn.' <span class="'.$txtColorEn.'">{menu-lang-en}</a></li>
    </ul>
    </li>
    ';

    return $html;
}



/**
* Ajoute dans le code HTML (en bas de page) le chargement 
* De la Clase JS Chart (nécessaire pour la réaliser des Charts)
* 
* @param string $pathTHEME
*/
function insertJsChart($pathTHEME)
{
    $html ='

    <!-- Chart.js - http://www.chartjs.org (necessary for Chart) -->
    <script src="'.$pathInstall.DS.$pathTHEME.D_CORE.DS.D_JS.DS.'chart'.DS.'chart.min.js"></script>

    ';

    return $html;
}


/**
* Ajoute dans le code HTML (en bas de page) le chargement des fichiers JS de base
* 
* @param string $pathTHEME
*/
function insertFooterJsSection($pathTHEME, $addOns = NULL)
{

    $html = '  
    <!-- jQuery (necessary for Bootstrap) -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'jquery'.DS.K_JQUERY_VER.DS.'jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="'.$pathTHEME.D_CORE.DS.D_BOOTS.DS.K_BOOTS_VER.DS.D_JS.DS.'bootstrap.min.js"></script>
    <!-- Codemirror-->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'lib'.DS.'codemirror.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'htmlmixed'.DS.'htmlmixed.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'xml'.DS.'xml.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'javascript'.DS.'javascript.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'css'.DS.'css.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'clike'.DS.'clike.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'php'.DS.'php.js"></script>    
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'lua'.DS.'lua.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'haxe'.DS.'haxe.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'python'.DS.'python.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'sql'.DS.'sql.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'mode'.DS.'swift'.DS.'swift.js"></script>

    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'addon'.DS.'selection'.DS.'active-line.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'addon'.DS.'edit'.DS.'matchbrackets.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'codemirror'.DS.'addon'.DS.'display'.DS.'fullscreen.js"></script>
    <!-- Wal Js -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'wal'.DS.'wal.js"></script>   
    
    ';
    $html .= $addOns;            
    return $html;
}
/*
function insertFooterJsSection($pathTHEME, $addOns = NULL)
{

    $html = '  
    <!-- jQuery (necessary for Bootstrap) -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'jquery'.DS.K_JQUERY_VER.DS.'jquery.min.js"></script>
    <!-- jQueryUI -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'jqueryui'.DS.K_JQUERYUI_VER.DS.'jquery-ui.min.js"></script>
    <!-- Bootstrap -->
    <script src="'.$pathTHEME.D_CORE.DS.D_BOOTS.DS.K_BOOTS_VER.DS.D_JS.DS.'bootstrap.min.js"></script>
    <!-- Hightlighter-->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'highlight'.DS.'highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <!-- Wal Js -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'wal'.DS.'wal.js"></script>   
    
    ';
    $html .= $addOns;            
    return $html;
}
*/

/**
* Ajoute dans le code HTML (en bas de page) le chargement des fichiers JS du
* module dataTables de jQuery 
* 
* @param string $pathTHEME
*/
function insertFooterJsDatatables($pathTHEME)
{

    $html = '  
    <!-- DataTables for jQuery -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'datatables'.DS.'datatables.custom.js"></script> 
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'datatables'.DS.'datatables.bootstrap.min.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'wal'.DS.'datatable.js"></script>  
            
    ';
    return $html;
}

/**
* Retourne une div formatée en fonction des données reçues
* La variable innerSpan permet d'inclure une balise de type span qui
* permet d'afficher un message secondaire
* @param string $type  (ALERT, SUCCESS, WARNING)
* @param string $classes
* @param string $message
* @param string $role
* @param string $id
* @param string $innerSpan
*/
function KTMakeDiv( $type, $classes, $message, $role = null, $id = null, $innerSpan = null )
{
    Switch($type)
    {
        case 'ALERT':
            $div = '<div class="';           
            $div .= $classes.'"';
            $div .= 'id="'.$id.'" ';
            $div .= ' role="'.$role.'">';
            $div .= $message;
            if(isset($innerSpan)) $div .= '<span id="submsg">'.$innerSpan.'</span>';
            $div .= '</div>';

        case 'SUCCESS':
            $div = '<div class="';
            $div .= $classes.'" ';
            $div .= 'id="'.$id.'" ';
            $div .= ' role="'.$role.'">';
            $div .= $message;
            if(isset($innerSpan)) $div .= '<span id="submsg">'.$innerSpan.'</span>';
            $div .= '</div>';

        case 'WARNING':
            $div = '<div class="';
            $div .= $classes.'"';
            $div .= 'id="'.$id.'" ';
            $div .= ' role="'.$role.'">';
            $div .= $message;
            if(isset($innerSpan)) $div .= '<span id="submsg">'.$innerSpan.'</span>';
            $div .= '</div>';
    }


    return $div;
}

/**
* Retourne la string de la fenêtre modale de suppression
* 
*/
function displayModalDelete($titre, $phraseConfirmation)
{

    $string='
    <div id="delete-modal" class="modal fade mTop100" tabindex="-1" role="dialog" aria-labelledby="{trm-supprimer-user}" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-trash-o"></i> '.$titre.'</h4>
    </div>

    <form class="form-inline" id="delete-action" action="" role="form" method="POST">
    <div class="modal-body">
    <p class="text-center ">'.$phraseConfirmation.'</p>
    <p class="text-center"><span id="id-to-delete" class="wGreenColor"></span></p>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{trm-annuler}</button>
    <button type="submit" class="btn btn-danger">{trm-supprimer}</button>
    </div>
    </form>

    </div>
    </div>
    </div>
    ';

    return $string;

}

