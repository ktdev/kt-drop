<?php

/**
* Code(s) - Class UsersCodes
* Gestion et manipulations des users
* 
* Author: ken@kentaro.be  - www.ktdev.pro 
* Under Licence MIT
*/

class UsersCodes
{
    
    private $rolesAll = null;
    private $droitsAll = null;
    private $statusAll = null;
    private $usersDirectory = NULL;
    private $status = NULL;
    private $usersArray = array();
    private $userArray = array();

    public function __construct()
    {
           
        // Initialisation des rôles
        require KT_ROOT.DS.D_PRIVATE.DS.D_PARAMS.DS.'roles.php';
        $this->rolesAll = $rolesAll;
        $this->droitsAll = $droitsAll;

        // Initialisation des status
        require KT_ROOT.DS.D_PRIVATE.DS.D_PARAMS.DS.'status.php';
        $this->statusAll = $status;

        // Initialisations du répertoires des utilisateurs
        $this->usersDirectory = KT_ROOT.DS.D_PRIVATE.DS.D_USERS.DS;

        // Chargement des données
        $this->KTloadUsers($this->usersDirectory);
    }

    /**
    * Chargement des données de tous les utilisateurs
    * Crée un array des utilisateurs en récupérant les données des fichiers ini
    * 
    * @param string $usersDirectory: Chemin du répertoire des utilisateurs
    */
    private function KTloadUsers($usersDirectory)
    {

        $index = 0;

        // Si il est possible d'ouvrir le répertoire
        if( $dossier = @opendir($usersDirectory) ) {
            // Compte le nombre de fichiers
            $nbFiles = count(scandir($usersDirectory));

            // Si seules les entrées '.' et '..' (2 fichiers) sont présentes -> le répertoire est vide, on affiche un message
            if($nbFiles <= 2) {

                // Le répertoire Datas est vide
                $this->status = 2;

            }else { 
                //$this->status = 1;
                while( false !== ($fichier = readdir( $dossier )) )
                {

                    $info = new SplFileInfo($fichier);
                    $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

                    if( $fichier != '.' && $fichier != '..' && $extension == 'ini')
                    {

                        $this->usersArray[$index]['LOGIN'] = trim(getItemIniFile( $this->usersDirectory.$fichier, 'LOGIN', 'parameters' ));
                        $this->usersArray[$index]['EMAIL'] = trim(getItemIniFile( $this->usersDirectory.$fichier, 'EMAIL', 'parameters' ));
                        $this->usersArray[$index]['PASSWORD'] = trim(getItemIniFile( $this->usersDirectory.$fichier, 'PASSWORD', 'parameters' ));
                        $this->usersArray[$index]['ROLE'] = trim(getItemIniFile( $this->usersDirectory.$fichier, 'ROLE', 'parameters' ));
                        $this->usersArray[$index]['STATUS'] = trim(getItemIniFile( $this->usersDirectory.$fichier, 'STATUS', 'parameters' ));
                                              
                        $index ++;

                    }

                }

                // Fermeture du dossier    
                closedir( $dossier );
            }
        }else {

            // Il n'est pas possible d'accéder au répertoire Datas
            $this->status = 3;

        } 
    } // End function KTloadUsers()

    /**
     * Chargement des données d'un utilisateur
     */
    public function KTLoadUser($filename)
    {
        $this->userArray['LOGIN'] = trim(getItemIniFile( $this->usersDirectory.$filename, 'LOGIN', 'parameters' ));
        $this->userArray['EMAIL'] = trim(getItemIniFile( $this->usersDirectory.$filename, 'EMAIL', 'parameters' ));
        $this->userArray['PASSWORD'] = trim(getItemIniFile( $this->usersDirectory.$filename, 'PASSWORD', 'parameters' ));
        $this->userArray['ROLE'] = trim(getItemIniFile( $this->usersDirectory.$filename, 'ROLE', 'parameters' ));
        $this->userArray['STATUS'] = trim(getItemIniFile( $this->usersDirectory.$filename, 'STATUS', 'parameters' ));

    } // End function KTloadUser()

    /**
     * Créer une liste (format html) des utilisateurs sous la forme d'un tableau de gestion (edit/delete compris)
     */
    public function KTtableCodesUsers($usersArray)
    {
        
        $str = null;
        $id = 0;
        $str .= '<div class="table-responsive mTop20"><table class="table">';
        $str .= '
        <thead><tr>
        <th>ID</th><th>Login</th><th>Email</th><th>Role</th><th>Status</th><th>Edit</th><th>Del</th>
        </tr></thead><tbody>';
        foreach($usersArray as $user)
        {
            $str .= "<tr>";
            $str .= '<td>'.$id.'</td>';
            foreach($user as $param => $value)
            {
                // On Sélectionne les données à afficher
                if($param == 'LOGIN' || $param == 'EMAIL')
                    $str .= '<td>'.$value.'</td>';

                // Résolution des rôles
                if($param == 'ROLE')
                {
                    foreach($this->rolesAll as $key => $val)
                    {
                        if ($value == $key){
                            $value = $val;
                            $str .= '<td>'.$value.'</td>';
                        }                           
                    }
                } 

                // Résolution des rôles
                if($param == 'STATUS')
                {
                    foreach($this->statusAll as $key => $val)
                    {
                        if ($value == $key){
                            $value = $val;
                            $str .= '<td>'.$value.'</td>';
                        }                           
                    }
                } 
                
               // On sauvegarde le login pour l'édition et la suppression
               if($param == 'LOGIN')
                  $login = $value;

            }
            $str .= '<td><a href="gestion-users?act=edit&login='.strtoupper($login).'"><i class="fa fa-pencil-square-o fa-lg suite-icons" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Editer"></i></a></td>';
            $str .= '<td><a class="deleteUser" data-id="'.strtoupper($login).'" href="#"><i class="fa fa-trash fa-lg suite-icons el_error deleteUser" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Supprimer"></i></a></td>';
            $str .= '</tr>';

            $id++;
        }
        $str .= '</tbody></table></div>';
        return $str;
    }// END KTtableCodesUsers()

    /**
     * Crée  une list de sélection (format html) des rôles
     */
    public function KTSelectRoles($selected = null)
    {
        $str = null;
        //sort($this->rolesAll);
        $str .= ' <select class="form-control" id="roles" name="roles">';
        foreach($this->rolesAll as $id => $name)
        {
            if($selected == $id)
                $str .= '<option value="'.$id .'" selected>'.$name.'</option>';
            else
                $str .= '<option value="'.$id .'">'.$name.'</option>';
        }
        $str .= ' </select>';
        return $str;

    }

     /**
     * Crée  une list de sélection (format html) des status
     */
    public function KTSelectStatus($selected = null)
    {
        $str = null;
        //sort($this->statusAll);
        $str .= ' <select class="form-control" id="status" name="status">';
        foreach($this->statusAll as $id => $name)
        {
            if($selected == $id)
                $str .= '<option value="'.$id .'" selected>'.$name.'</option>';
            else
                $str .= '<option value="'.$id .'">'.$name.'</option>';
        }
        $str .= ' </select>';
        return $str;

    }

    /**
    * Fonction d'identification des utilisateurs sur l'application
    * avec le système de fichiers
    *
    * @param mixed $user
    * @param mixed $passwd
    */
    public function KTIdentUserWithFile($user, $passwd)
    {
        $st =  array();

        $users = new UsersCodes();
        $user = $users->KTLoadUser($user.'.ini');
        $userInfos = $users->KTgetUser();
        $hashPasswd = $userInfos['PASSWORD'];

        $verify = password_verify($passwd, $hashPasswd);

        // Si correspondance
        if ($verify == true) {
            
            // Si actif
            $statut = $userInfos['STATUS'];
            if ($statut) {
                $st['stat'] = TRUE;
                $st['msg'] = T_('Vous êtes à présent connecté');
                $st['pseudo'] = $userInfos['LOGIN'];
                $st['email'] = $userInfos['EMAIL'];
                $st['actif'] = $userInfos['STATUS'];
                $st['role'] = $userInfos['ROLE'];

                return $st;
            } else {
                $st['stat'] = FALSE;
                $st['msg'] = T_('Votre compte utilisateur est inactif, veuillez prendre contact avec votre administrateur');
                return $st;
            }
        } else {
            $st['stat'] = FALSE;
            $st['msg'] = T_('Votre login et/ou votre mot de passe sont erronés - Veuillez réessayer');
            return $st;
        }
    }

    /**
     * Getter du tableau de TOUS les utilisateurs
     */
    public function KTgetUsers()
    {
        return $this->usersArray;  
    } // End function KTgetUsers()

    /**
     * Getter du tableau d'un utilisateur, précédemment chargé
     * avec la fonction KTLoadUser($filename)
     */
    public function KTgetUser()
    {
        return $this->userArray;  
    } // End function KTgetUser()

    /**
     * Getter des rôles utilisateur
     */
    public function KTgetRolesAll()
    {
        return $this->rolesAll;  
    } // End function KTgetRolesUsers()

    /**
     * Getter des status utilisateur
     * Retourne l'ensemble des status
     */
    public function KTgetStatusAll()
    {
        return $this->statusAll;  
    } // End function KTgetStatusAll()

    /**
     * Getter de la propriété $status
     */
    public function KTgetStatus()
    {

        return $this->status;  

    } // End function KTgetStatus()

    
    /**
     * Getter de la propriété $droitsAll
     * Retourne l'ensemble des droits
     */
    public function KTgetDroitsAll()
    {

        return $this->droitsAll;  

    } // End function KTgetDroitsAll()

    /**
     * Retourne le type d'accès à la ressource (module) en fonction du rôle utilisateur
     */
    public function KTgetAccessModule($module, $UserRole)
    {
        $droitsAll = $this->droitsAll;

        foreach($droitsAll as $users => $modules)
        {
           if($users == $UserRole)
           {
                foreach($modules as $modName => $modDroit)
                {
                    
                    if($modName == $module)
                        return $modDroit;
                }
           }
            
        }

    } // End function KTgetAccessModule()
}