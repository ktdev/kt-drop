<?php


//-----------------------------------------------------------------------------
class debug {

    // Activation ou désactivation du mode Debug
    // Activez le mode Debug en Développement et désactivez le mode en Production

    public static $debugMode = K_DEBUG;

    static function KTDebug($variable, $nomVariable = null) {

        if (self::$debugMode) {

            $debug = debug_backtrace();

            echo '<div id="ktdebug">';

            echo '<hr>';
            echo '<h3> Debug informations</h3>';

            echo '<p><a href="#" onclick="$(this).parent().next(\'ol\').slideToggle(); return false;" ><strong> ' . $debug[0]['file'] . ' </strong>: Ligne: ' . $debug[0]['line'] . '</a></p>';
            echo '<ol style="display: none;">';

            foreach ($debug as $key => $value) {
                if ($key > 0) {
                    echo '<li><strong> ' . $value['file'] . ' </strong>: Ligne: ' . $value['line'] . '</li>';
                }
            }

            echo '</ol>';

            echo '<p>Valeur de: ' . $nomVariable . '</p>';
            echo '<pre>';
            print_r($variable);
            echo '</pre>';
            echo '<hr>';

            echo '</div>';
        }
        die();
    }

    static function KTGenerate($startTime) 
    {

        if (self::$debugMode) {
            echo '
						<div style="position:fixed;bottom:0; background:#900; color:#FFF; line-height:30px; height:30px; left:0; right:0; padding: 0 10px; text-align: right; z-index:10000;">
                            DEBUG MODE ACTIVATED - This page is generate in: ' . round(microtime(true) - $startTime, 5) . ' sec
                        </div>
					';
        }
    }




    static function KTprint_r($var)
    {
        $string = '<pre class="wGreenColor">';
        ob_start();
        print_r($var);
        $string .= ob_get_clean();
        $string .= '</pre>';
        return $string;
    }

// End of class
}
