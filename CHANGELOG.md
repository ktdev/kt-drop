# Changelog Code(s)

-------
* V 1.3.0 : 25-07-2019
    * [NEW] Ajout de la fonctionnalité infos (auteur et date de mise à jour)
    * [NEW] Ajout d'un classement par étiquette (tag)


* V 1.2.0 : 19-07-2019  
  * [FIX] Rémanence des champs si génération d'une erreure lors de la création d'un snippet    
  * [NEW] Ajout de l'exportation et de l'importation des données de l'application (fonctionne uniquement sur les serveurs GNU/Linux uniquement)
  
      
* V 1.1.0 : 12-07-2019  
  * Première release

