
body{ 
    font-size: 14px;  
    background: #25231f;
    color: rgb(219, 210, 184);
}
h3{
    color: rgb(231, 192, 16) !important;  
    font-size: 2rem;
}
h4{ 
    color: rgb(231, 192, 16);  
    margin: 50px  0 50px 0; 
    border: 1px solid rgb(231, 192, 16);  
    border-radius: 0.5rem;
    padding: 1rem;
}
h5{
     font-weight: bold; 
} 
