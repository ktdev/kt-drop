<!doctype html>

<html lang="fr">
<head>
  <meta charset="utf-8">

  <title>Template de base</title>
  <meta name="description" content="Template de base">
  <meta name="author" content="Me">
  
  <link rel="stylesheet" href="css/styles.css">

</head>
<body>
  
  <script src="js/scripts.js"></script>
  
</body>
</html>