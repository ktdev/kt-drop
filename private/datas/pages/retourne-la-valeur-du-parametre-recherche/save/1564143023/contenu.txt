<?php
/**
* Retourne la valeur du paramètre recherché
* @param unknown $array
* @param unknown $key
* @return unknown
* test
*/
function KTFindParam($array, $key) {
    foreach ($array as $param) {
        if (in_array($key, $param)) {
            return $param['value'];
        }
    }

    return FALSE;
}
?>