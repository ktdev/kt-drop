$(document).ready(function(){
        var codeEditorElement = $(".codemirror-textarea")[0];
        var editor = CodeMirror.fromTextArea(codeEditorElement, {
            mode: "javascript",
            lineNumbers: true,
            matchBrackets: true,
            theme: "material",
            lineWiseCopyCut: true,
            styleActiveLine: true,
            extraKeys: {
                "F11": function(cm) {
                  cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function(cm) {
                  if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
              },
            undoDepth: 200            
          });
        editor.setSize("100%","100%") // Width, Height of editor 
 
    });