<?php

// Rôles des utilisateurs
$rolesAll = [
    '1000' => 'Administrateur',
    '1' => 'Simple utilisateur'
    ];

// DROITS AUTORISES : |nom du module| => |NO| = 0 / |READ| = 1 / |WRITE| = 2 / |FULL| = 3   
$droitsAll = [

    // Administrateur
    '1000' => [
        'code' => 3,
        'adm' => 3,
        'home' => 3,
        'user' => 3,
        '_menu' => 3 
    ],

    // Simple utilisateur
    '1' => [
        'code' => 1,
        'adm' => 0,
        'home' => 3,
        'user' => 3, 
        '_menu' => 1
    ]
];