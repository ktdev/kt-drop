<?php

//-----------------------------------------------------------------------------

     // Définit le surnom de l'application
     if(!defined('K_SURNAME')) define( 'K_SURNAME','Snippets Farm');

    // Définit la langue par défaut (fr_FR, en_US)
    if (! defined ( 'DEF_LOCALE' )) define ( 'DEF_LOCALE', 'fr_BE' );    

     // Système d'identification des utilisateurs (FILE or DB)
     if (! defined ( 'K_INDENT_SYS' )) define ( 'K_INDENT_SYS', 'FILE' ); 

    // Active ou désactive le mode Debug (TRUE or FALSE)
    if (! defined ( 'K_DEBUG' )) define ( 'K_DEBUG', false );     

    // Configure le niveau d'erreurs en fonction du mode de debugging
    if (K_DEBUG) error_reporting(E_ALL | E_STRICT); else error_reporting(0);

    // Temps d'attente (en seconde(s)) avant le raffraîchissement automatique d'une page après l'envoi d'un formulaire
    if (! defined ( 'K_REFRESH' )) define ( 'K_REFRESH', 1 ); 

    
    
    
   
   
  