<?php

// Edit this array as needed  - Editer ce tableau selon vos besoins
$tags = [
    '0' => 'Php',
    '1' => 'CSS',
    '2' => 'Javascript',
    '3' => 'Html',
    '4' => 'Note',
    '5' => 'Text',
    '6' => 'Markdown',
    '7' => 'Sql',
    '8' => 'C',
    '9' => 'C++',
    '10' => 'C#',
    '11' => '.net',
    '12' => 'Java',
    '13' => 'Python',
    '14' => 'Lua',
    '15' => 'Swift'    
    ];