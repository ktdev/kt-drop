<?php

//-----------------------------------------------------------------------------
class ConfDB
{

    // Credentials Databases
    // Plusieurs Credentials sont possibles
    static $databases = array(

        'default' => array(
            'host' => 'localhost',
            'port' => '3306',
            'database' => 'code',
            'prefix' => '',
            'login' => '',
            'password' => ''
        )
    );
} // End Conf Class
