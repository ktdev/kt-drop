﻿
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $(document).ready(function(){
        var codeEditorElement = $(".codemirror-textarea")[0];
        var editor = CodeMirror.fromTextArea(codeEditorElement, {
           // mode: "application/x-httpd-php",
           mode: "javascript",
            lineNumbers: true,
            matchBrackets: true,
            theme: "material",
            lineWiseCopyCut: true,
            styleActiveLine: true,
            extraKeys: {
                "F11": function(cm) {
                  cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function(cm) {
                  if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
              },
            undoDepth: 200            
          });
        editor.setSize("100%","100%") // Width, Height of editor 
 
    });

/* +++++++++++++++++++++++++++++++++++++++++++ */
/* +++ Traitement de la Modale Delete User +++ */
/* +++++++++++++++++++++++++++++++++++++++++++ */
$('.deleteUser').click(function(){    
    
    // Vidage de l'url avant l'écriture pour éviter les multis
    $('#id-to-delete').empty();

    // Ob capture la chaîne de caractères de l'attribut data-id 
    var login = $(this).attr('data-id');  
    var url = "gestion-users?act=del&login="+login;
    
    // On écrit les données dans la modale  
    $('#delete-action').attr("action", url);
    $('#id-to-delete').append(login);
   
    /* On affiche la Modale Delete */
    $('#delete-modal').modal('show');

});

/* ++++++++++++++++++++++++++++++++++++++++++++++ */
/* +++ Traitement de la Modale Delete Snippet +++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++ */
$('.deleteSnip').click(function(){    
    
    // Vidage de l'url avant l'écriture pour éviter les multis
    $('#id-to-delete').empty();

    // Ob capture la chaîne de caractères de l'attribut data-id 
    var id = $(this).attr('data-id');  
    var url = "adm-pages?delid="+id;
    
    // On écrit les données dans la modale  
    $('#delete-action').attr("action", url);
    $('#id-to-delete').append(id);
   
    /* On affiche la Modale Delete */
    $('#delete-modal').modal('show');

});

