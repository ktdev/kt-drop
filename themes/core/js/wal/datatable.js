/*
 * Exemple d'utilisation de datatable
*/

/*
$(document).ready(function () {

    // On page load: datatable
    var table_articles = $('#table_prisoners').dataTable({
        "ajax": "../../../app/mods/alpha/listing?job=get_article",
        "language": {
            "decimal": ",",
            "thousands": "."},
        "columns": [
            {"data": "id"},
            {"data": "number"},
            {"data": "date"},
            {"data": "titre"},
            {"data": "contenu"},
            {"data": "auteur"},
            {"data": "groupe"},           
            {"data": "statut"},
            {"data": "functions", "sClass": "functions"}
        ],
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [-1]}
        ],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "oLanguage": {
            "oPaginate": {
                "sFirst": "First",
                "sPrevious": "Previous ",
                "sNext": "Next",
                "sLast": "Last",
            },
            "sLengthMenu": "Records per page: _MENU_",
            "sInfo": "Total of _TOTAL_ records (showing _START_ to _END_)",
            "sInfoFiltered": "(filtered from _MAX_ total records)"
        }
    });

});

*/
