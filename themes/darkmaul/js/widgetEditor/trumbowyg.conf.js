$('#contenu').trumbowyg({
    svgPath: '../../../themes/wally/css/widgetEditor/icons.svg',
    lang: 'fr',
    btns: [
        ['viewHTML'],
        ['undo', 'redo'], // Only supported in Blink browsers
        ['formatting'],
        ['strong', 'em', 'del'],
        ['link'],
        ['insertImage'],
        ['preformatted'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ]
});
