


/* Déclaratione et intialisations globales */
var counter = 3;
var intervalId = null;


/**
* Génération aléatoire d'un nombre
* Par défaut sur 4 chiffres
* 
* @param {Number} l
*/
function KTrands(l)
{

    var result='';
    if (typeof l==='undefined'){var l = 4;}    
    for(var i=0; i<l; ++i)
    {
        /* on insère un caractère alphanumérique aléatoire */
        result += Math.round(Math.random() * 9);
    }       
    //result = parseInt(result);                                
    return result;
}

function countDown()
{
  document.getElementById('submsg').innerHTML = "  (" + counter + " sec)";
  counter--;
}
function action()
{
  clearInterval(intervalId);  
}


$(document).ready(function() {

    $(document).on('click','#regenerate-pwd', function(e){
        e.preventDefault();
        $("input[name='password']").val(KTrands());
    });
    
    $(document).on('click','#regenerate-login', function(e){
        e.preventDefault();
        $("input[name='login']").val(KTrands(6));        
    });
    
    // Si un élément cronos est découvert on lance un timer (ex: prisonner/update.php)
    if(document.getElementById('cronos')) {
       intervalId = setInterval(countDown, 1000);
       setTimeout(action, counter * 1000);
    }
    

});