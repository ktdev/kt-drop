# Code(s) : by KTDev

Version : 1.3.0

Code(s) est développé sous le principe KISS : Keep it simple, stupid

## Installation

### Manuelle (uniquement):  

* Désarchiver l'archive de l'application
* Copier l'ensemble des fichiers de l'archive sur votre serveur (hormis le répertoire _src, ce dernier n'étant pas nécessaire) 
* Au préalable, veuillez vérifier que le module de réécriture d'url de votre serveur Apache, le 'rewrite_module' soit activé.
* Editer la ligne 'RewriteBase' du fichier .htaccess. Par défaut intialiser avec un / convient si vous installer l'application
 à la racine de votre hébergement. Dans le cas contraire donner le chemin de votre installation ex.: /dir1/dir2/code
* Modifier le fichier /private/params/path-install.php et indiquez le même chemin d'installation que dans le RewriteBase ddu
fichier .htaccess configuré précédemment.
* Code(s) est à présent installé


### Informations complémentaires

Code(s) n'utilise pas de base de données pour sauvegarder vos données. Celles-ci sont sauvegardées dans des fichiers directement sur le
système de fichiers de votre serveur.  Par défaut deux utilisateurs sont présents dans la configuration de base de l'application :

* Login : Admin -  Password : Admin123; [ Compte Administrateur : accès complet ]
* Login : Guest - Password : Guest123; [ Compte Guest : accès invité - consultation uniquement]

Les données sont situées sous le dossier :  private/datas/
Les paramètres de votre installation sous le dossier : private/params/
Les utilisateurs sous le dossier : private/users/

**Attention Code(s) est  développé comme une application privée, il faut être identifié pour visualiser son contenu et effectuer des modifications.**

### Mises à jour

Copier tous les fichiers de la nouvelles archive, **sauf** :

Le dossier **private/**
Le fichier **.htaccess** situé à la racine de l'application



Développé par KTDev - www.ktdev.pro
Contact : ken@kentaro.be

